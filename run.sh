#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

BUILD_DIR=${SCRIPT_DIR}/build

UTS_RUN_COMMAND="$1"

if [ "${UTS_RUN_COMMAND}" = "--help" ] || [ "${UTS_RUN_COMMAND}" = "-h" ]; then
    echo "Syntax:"
    echo "$0 run|runDebug|<utName>_run|<utName>_runDebug"
    exit 0
fi

if [ -z "${UTS_RUN_COMMAND}" ]
then
      UTS_RUN_COMMAND=runDebug
fi

if [ ! -d ${BUILD_DIR} ]; then
  ${SCRIPT_DIR}/regenMakeFile.sh
fi

cd ${BUILD_DIR}

make ${UTS_RUN_COMMAND}

cd ${SCRIPT_DIR}
