#!/usr/bin/env bash 

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function ExitOnError {
    local errorCode=$1
    local errorMsg=$2
    if [ $errorCode -ne 0 ]
    then
        cd $SCRIPT_DIR
        echo "Error: $errorMsg"
        exit 1
    fi
}

sudo apt-get install build-essential autoconf gperf clang bison flex libreadline-dev gawk tcl-dev libffi-dev git mercurial graphviz xdot pkg-config python python3 libftdi-dev qt5-default python3-dev libboost-all-dev  cmake libeigen3-dev zlib1g zlib1g-dev
ExitOnError $? "Failed to install the required dependencies"

VERILOG_SIMULATION_TOOLS_DIR=~/Programs/VerilogSimulators

mkdir -p $VERILOG_SIMULATION_TOOLS_DIR
ExitOnError $? "Failed to create directory $VERILOG_SIMULATION_TOOLS_DIR"

ICARUS_IVERILOG_SRCS_DIR=$VERILOG_SIMULATION_TOOLS_DIR/icarus_iverilog/src
VERILATOR_SRCS_DIR=$VERILOG_SIMULATION_TOOLS_DIR/verilator/src

ICARUS_IVERILOG_BINS_DIR=$VERILOG_SIMULATION_TOOLS_DIR/icarus_iverilog/bin
VERILATOR_BINS_DIR=$VERILOG_SIMULATION_TOOLS_DIR/verilator/bin

ICARUS_IVERILOG_RELEASE_COMMIT_ID=453c5465895eaca4a792d18b75e9ec14db6ea50e # Release v10.3
VERILATOR_RELEASE_COMMIT_ID=95c4b6aaba789593d16a3b59ec6385e4478310b5 # Release v4.030

# Get sources
if [[ -d "$ICARUS_IVERILOG_SRCS_DIR/.git" ]]
then
    cd $ICARUS_IVERILOG_SRCS_DIR
    
    git fetch
    ExitOnError $? "Failed to fetch Icarus git repo"
    
    git checkout $ICARUS_IVERILOG_RELEASE_COMMIT_ID
    ExitOnError $? "Failed to checkout desired Verilator commit $ICARUS_IVERILOG_RELEASE_COMMIT_ID"
else
    ICARUS_IVERILOG_REPO_URL=https://github.com/steveicarus/iverilog.git
    
    git clone $ICARUS_IVERILOG_REPO_URL $ICARUS_IVERILOG_SRCS_DIR
    ExitOnError $? "Failed to clone $ICARUS_IVERILOG_REPO_URL"
    
    cd $ICARUS_IVERILOG_SRCS_DIR
    
    git checkout $ICARUS_IVERILOG_RELEASE_COMMIT_ID
    ExitOnError $? "Failed to checkout desired Verilator commit $ICARUS_IVERILOG_RELEASE_COMMIT_ID"
fi

if [[ -d "$VERILATOR_SRCS_DIR/.git" ]]
then
    cd $VERILATOR_SRCS_DIR

    git fetch
    ExitOnError $? "Failed to fetch Verilator git repo"
    
    git checkout $VERILATOR_RELEASE_COMMIT_ID
    ExitOnError $? "Failed to checkout desired Verilator commit $VERILATOR_RELEASE_COMMIT_ID"
else
    VERILATOR_REPO_URL=https://github.com/verilator/verilator.git
    
    git clone $VERILATOR_REPO_URL $VERILATOR_SRCS_DIR
    ExitOnError $? "Failed to clone $VERILATOR_REPO_URL"
    
    cd $VERILATOR_SRCS_DIR
    
    git checkout $VERILATOR_RELEASE_COMMIT_ID
    ExitOnError $? "Failed to checkout desired Verilator commit $VERILATOR_RELEASE_COMMIT_ID"
fi

cd $SCRIPT_DIR

# Build the tools
cd $ICARUS_IVERILOG_SRCS_DIR
sh autoconf.sh
ExitOnError $? "Icarus build Autoconf step failed"
sh configure --prefix=$ICARUS_IVERILOG_BINS_DIR
ExitOnError $? "Icarus build configuration failed"
make clean
ExitOnError $? "Could not clean Icarus build"
make -j$(nproc)
ExitOnError $? "Icarus build failed"
make check
ExitOnError $? "Icarus build check failed"
make install
ExitOnError $? "Icarus build installation failed"

cd $VERILATOR_SRCS_DIR
unset VERILATOR_ROOT
autoconf
ExitOnError $? "Verilator build Autoconf step failed"
sh configure --prefix=$VERILATOR_BINS_DIR
ExitOnError $? "Verilator build configuration failed"
make clean
ExitOnError $? "Could not clean Verilator build"
make -j$(nproc)
ExitOnError $? "Verilator build failed"
make test -j$(nproc)
ExitOnError $? "Verilator build test failed"
make install
ExitOnError $? "Verilator build installation failed"


cd $SCRIPT_DIR
exit 0
