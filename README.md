# TCPU (Test CPU)

A simple single issue scalar register based processor which I wrote in Verilog for learning purposes.
It has been tested in simulation and on a [ICE40HX8K-B-EVN](http://www.latticesemi.com/~/media/LatticeSemi/Documents/UserManuals/EI/EB85.pdf) FPGA board.

For a software interface description see below.

# List of TCPU Verilog Source Files
```
src/TCPU/AddrDecoder.v
src/TCPU/ExecutionUnit.v
src/TCPU/GPIOModule.v
src/TCPU/InstructionDecoder.v
src/TCPU/RAMMemory.v
src/TCPU/RegisterAddressDecoder.v
src/TCPU/RegisterFile.v
src/TCPU/ROMData.v
src/TCPU/ROMMemory.v
src/TCPU/TCPU_Top.v
src/TCPU/TCPU.v
src/ALU/Adder.v
src/ALU/ALU_Test_Top.v
src/ALU/ALU_Test.v
src/ALU/ALU_Top.v
src/ALU/ALU_Top.vcd
src/ALU/ALU.v
src/ALU/Multiplier.v
src/ALU/MUX2.v
src/ALU/MUX3.v
src/ALU/MUX7.v
src/BarrelShifter/BarrelShifter.v
```

# List of TCPU Simulator Source Files
```
src/TCPU/TCPUSim.cpp
```

# Software Interface
World lenght: 32 bits

__Registers (32 bit wide):__
```
    r0,
    r1,
    r2,
    r3,
    r4,
    r5,
    r6,
    r7,
    r8,
    r9,
    r10,
    r11,
    r12,
    r13,
    r14,  
    r15  // Used as a Link Register
    r16  // Used as a Stack Pointer
    PC // Separate, non user writable or readable register
```


__Processor Flags:__
```
    CF - ALU op resulted in a carry
    ZF - ALU operation result is zero
    OF - ALU op resulted in an overflow (either negative or positive)
    NF - ALU op operation resulted in a negative number
```
    
    
__Exception Vector:__
```
    0x00000000 - Entry point addr
    0x00000004 - Invalid instruction
    0x00000008 - Invalid address
```

__Instructions:__
```
add r1, r2     // r1 = r1 + r2
sub r1, r2     // r1 = r1 - r2
mul r1, r2     // r1 = r1 * r2
inc r1         // Increment r1 by one
dec r1         // Decrement r1 by one
xor r1, r2     // r1 = r1 ^ r2
orr r1, r2     // r1 = r1 | r2
and r1, r2     // r1 = r1 & r2
not r1         // r1 = ~r1
shl r1, r2     // Shift Left
shr r1, r2     // Shift Right
cmp r1, r2     // Do r1 - r2, update the processor flags accordingly and then discard the result
jmp r1         // Write address of the memory location immediatelly following the jmp instruction to the r15 register
jeq r1         // If cmp instruction says that values where equal
jne r1         // If cmp instruction says that values where not equal
jgt r1         // If r1 > r2 in cmp instruction
jlt r1         // If r1 < r2 in cmp instruction
jge r1         // If r1 >= r2 in cmp instruction
jle r1         // If r1 <= r2 in cmp instruction
jmp #Const     // Jump to a 16bit constant address
jeq #Const     // Jump to a 16bit constant address if cmp instruction says that values where equal
jne #Const     // Jump to a 16bit constant address if cmp instruction says that values where not equal
jgt #Const     // Jump to a 16bit constant address if r1 > r2 in cmp instruction
jlt #Const     // Jump to a 16bit constant address if r1 < r2 in cmp instruction
jge #Const     // Jump to a 16bit constant address if r1 >= r2 in cmp instruction
jle #Const     // Jump to a 16bit constant address if r1 <= r2 in cmp instruction
mov r1, r2     // Copy r2 into r1
ldr r1, [r2]   // Load data from the address stored in r2 into r1
ldu r1, #Const // Load 16bit const to the upper 16bits of the r1
ldl r1, #Const // Load 16bit const to the lower 16bits of the r1
str r1, [r2]   // Store data from r1 into an address in r2
push r1        // Push value of the r1 register onto the descending stack (This is done by first decrementing the stack pointer by 4 and then storing the register value into the resulting address)
pop r1         // Pop a value from descending the stack and store into the r1 register (This is done by first storing the data from the location pointed to by the current stack pointer address into the register and then incrementing the stack pointer address by 4)
```
__Instruction format:__
```
-------+----------------------------------------------------+
| Name | OpCode |  arg0  |  arg1  | Reserved for future use |
-------+----------------------------------------------------+
| Bits | 31..27 | 26..22 | 21..5  |         4..0            |
-------+----------------------------------------------------+
       ^                                                    ^
       |---------------------- 32 bits ---------------------|
```
       
__Arg1 Format:__
```
---------+-----------------------------------------------------+
| Name   |                 Arg Type             |    Payload   |
---------+-----------------------------------------------------+
| Bits   |                     16               |    15..0     |
---------+-----------------------------------------------------+
| Values | 0 - Payload represents constant data | Const data   |
---------| 1 - Payload represents a register ID | or reg. ID   |
         -------------------------------------------------------
         ^                                                     ^
         |----------------------- 17 bits ---------------------|
```

__Instruction Op Codes:__
```
jmp  - 0x00
jeq  - 0x01
jne  - 0x02
jgt  - 0x03        
jlt  - 0x04
jge  - 0x05
jle  - 0x06
add  - 0x07
sub  - 0x08
mul  - 0x09
inc  - 0x0A
dec  - 0x0B
xor  - 0x0C
orr  - 0x0D
and  - 0x0E
not  - 0x0F
shl  - 0x10
shr  - 0x11
cmp  - 0x12    
mov  - 0x13
ldr  - 0x14
ldu  - 0x15
ldl  - 0x16
str  - 0x17
push - 0x18
pop  - 0x19
```
 
