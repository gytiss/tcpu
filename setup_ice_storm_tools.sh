#!/usr/bin/env bash 

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

sudo apt-get install build-essential clang bison flex libreadline-dev gawk tcl-dev libffi-dev git mercurial graphviz xdot pkg-config python python3 libftdi-dev qt5-default python3-dev libboost-all-dev cmake libeigen3-dev

FPGA_TOOLS_DIR=~/Programs/LatticeFPGATools

mkdir -p $FPGA_TOOLS_DIR

ICE_STORM_SRCS_DIR=$FPGA_TOOLS_DIR/icestorm/src
ARACHNE_PNR_SRCS_DIR=$FPGA_TOOLS_DIR/arachne-pnr/src
NEXTPNR_SRCS_DIR=$FPGA_TOOLS_DIR/nextpnr/src
YOSYS_SRCS_DIR=$FPGA_TOOLS_DIR/yosys/src

ICE_STORM_BINS_DIR=$FPGA_TOOLS_DIR/icestorm/bin
ARACHNE_PNR_BINS_DIR=$FPGA_TOOLS_DIR/arachne-pnr/bin
NEXTPNR_BINS_DIR=$FPGA_TOOLS_DIR/nextpnr/bin
YOSYS_BINS_DIR=$FPGA_TOOLS_DIR/yosys/bin

# Get sources
if [[ -d "$ICE_STORM_SRCS_DIR/.git" ]]
then
    cd $ICE_STORM_SRCS_DIR
    git pull
else
    git clone https://github.com/cliffordwolf/icestorm.git $ICE_STORM_SRCS_DIR
fi

if [[ -d "$ARACHNE_PNR_SRCS_DIR/.git" ]]
then
    cd $ARACHNE_PNR_SRCS_DIR
    git pull
else
    git clone https://github.com/cseed/arachne-pnr.git $ARACHNE_PNR_SRCS_DIR
fi

if [[ -d "$NEXTPNR_SRCS_DIR/.git" ]]
then
    cd $NEXTPNR_SRCS_DIR
    git pull
else
    git clone https://github.com/YosysHQ/nextpnr $NEXTPNR_SRCS_DIR
fi

if [[ -d "$YOSYS_SRCS_DIR/.git" ]]
then
    cd $YOSYS_SRCS_DIR
    git pull
else
    git clone https://github.com/YosysHQ/yosys $YOSYS_SRCS_DIR
fi

cd $SCRIPT_DIR

# Build the tools
cd $ICE_STORM_SRCS_DIR
make clean
make -j$(nproc) PREFIX=$ICE_STORM_BINS_DIR
make install PREFIX=$ICE_STORM_BINS_DIR


cd $ARACHNE_PNR_SRCS_DIR
make clean
make -j$(nproc) PREFIX=$ARACHNE_PNR_BINS_DIR ICEBOX=$ICE_STORM_BINS_DIR/share/icebox
sudo make install PREFIX=$ARACHNE_PNR_BINS_DIR ICEBOX=$ICE_STORM_BINS_DIR/share/icebox


cd $NEXTPNR_SRCS_DIR
make clean
cmake -DARCH=ice40 -DCMAKE_INSTALL_PREFIX=$NEXTPNR_BINS_DIR -DEXTERNAL_CHIPDB_ROOT=$NEXTPNR_BINS_DIR/share/nextpnr -DICEBOX_ROOT=$ICE_STORM_BINS_DIR/share/icebox
make -j$(nproc)
sudo make install


cd $YOSYS_SRCS_DIR
make clean
make -j$(nproc) PREFIX=$YOSYS_BINS_DIR
sudo make install PREFIX=$YOSYS_BINS_DIR
