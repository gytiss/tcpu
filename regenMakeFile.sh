#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mkdir -p ${SCRIPT_DIR}/build 
cd ${SCRIPT_DIR}/build
cmake -G "Unix Makefiles" -DVERILATOR_ROOT=~/Programs/VerilogSimulators/verilator/bin -DCMAKE_BUILD_TYPE=Debug ../
cd ${SCRIPT_DIR}
