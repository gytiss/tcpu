module  InstructionDecoder
#(
    parameter REGISTER_INDEX_SP        = 0,
    parameter INSTRUCTION_OPCODE_PUSH  = 0,
    parameter INSTRUCTION_OPCODE_POP   = 0,
    
    parameter MIN_OP_CODE = 0,
    parameter MAX_OP_CODE = 0,
    
    parameter MIN_REG_ID = 0,
    parameter MAX_REG_ID = 0
)
(
    input wire [26:0] i_instruction,

    output wire [4:0]  o_opCode, 
    output wire [4:0]  o_arg0, 
    output wire [15:0] o_arg1, 
    output wire        o_arg1IsAReg,
    output wire        o_instructionIsValid
);

    assign o_opCode = i_instruction[26:22];
    assign o_arg0 = i_instruction[21:17];
    assign o_arg1 = (((o_opCode == INSTRUCTION_OPCODE_PUSH) | (o_opCode == INSTRUCTION_OPCODE_POP)) ? {11'b0, REGISTER_INDEX_SP} : i_instruction[15:0]);
    assign o_arg1IsAReg = (((o_opCode == INSTRUCTION_OPCODE_PUSH) | (o_opCode == INSTRUCTION_OPCODE_POP)) ? 1'b1 : i_instruction[16]);

/* verilator lint_off UNSIGNED */
    assign o_instructionIsValid = (((o_opCode >= MIN_OP_CODE) && (o_opCode <= MAX_OP_CODE)) && 
                                   ((o_arg0 >= MIN_REG_ID) && (o_arg0 <= MAX_REG_ID)) &&
                                   (((o_arg1 >= {11'b0, MIN_REG_ID}) && (o_arg1 <= {11'b0, MAX_REG_ID}) && (o_arg1IsAReg == 1'b1)) || (o_arg1IsAReg == 1'b0)));
/* verilator lint_on UNSIGNED */

endmodule
