`default_nettype none   // Don't allow undeclared nets

module RegisterAddressDecoder
#
(
    parameter REGISTER_INDEX_R0   = 0,
    parameter REGISTER_INDEX_R1   = 0,
    parameter REGISTER_INDEX_R2   = 0,
    parameter REGISTER_INDEX_R3   = 0,
    parameter REGISTER_INDEX_R4   = 0,
    parameter REGISTER_INDEX_R5   = 0,
    parameter REGISTER_INDEX_R6   = 0,
    parameter REGISTER_INDEX_R7   = 0,
    parameter REGISTER_INDEX_R8   = 0,
    parameter REGISTER_INDEX_R9   = 0,
    parameter REGISTER_INDEX_R10  = 0,
    parameter REGISTER_INDEX_R11  = 0,
    parameter REGISTER_INDEX_R12  = 0,
    parameter REGISTER_INDEX_R13  = 0,
    parameter REGISTER_INDEX_R14  = 0,
    parameter REGISTER_INDEX_LR   = 0,
    parameter REGISTER_INDEX_SP   = 0,

    parameter REGISTER_ADDRESS_R0  = 0,
    parameter REGISTER_ADDRESS_R1  = 0,
    parameter REGISTER_ADDRESS_R2  = 0,
    parameter REGISTER_ADDRESS_R3  = 0,
    parameter REGISTER_ADDRESS_R4  = 0,
    parameter REGISTER_ADDRESS_R5  = 0,
    parameter REGISTER_ADDRESS_R6  = 0,
    parameter REGISTER_ADDRESS_R7  = 0,
    parameter REGISTER_ADDRESS_R8  = 0,
    parameter REGISTER_ADDRESS_R9  = 0,
    parameter REGISTER_ADDRESS_R10 = 0,
    parameter REGISTER_ADDRESS_R11 = 0,
    parameter REGISTER_ADDRESS_R12 = 0,
    parameter REGISTER_ADDRESS_R13 = 0,
    parameter REGISTER_ADDRESS_R14 = 0,
    parameter REGISTER_ADDRESS_LR  = 0,
    parameter REGISTER_ADDRESS_SP  = 0
)
(
    input wire [4:0] i_regIndex,
    output reg [31:0] o_regAddress
);

    always_comb
    begin
        case(i_regIndex)
            REGISTER_INDEX_R0  : o_regAddress = REGISTER_ADDRESS_R0;
            REGISTER_INDEX_R1  : o_regAddress = REGISTER_ADDRESS_R1;
            REGISTER_INDEX_R2  : o_regAddress = REGISTER_ADDRESS_R2;
            REGISTER_INDEX_R3  : o_regAddress = REGISTER_ADDRESS_R3;
            REGISTER_INDEX_R4  : o_regAddress = REGISTER_ADDRESS_R4;
            REGISTER_INDEX_R5  : o_regAddress = REGISTER_ADDRESS_R5;
            REGISTER_INDEX_R6  : o_regAddress = REGISTER_ADDRESS_R6;
            REGISTER_INDEX_R7  : o_regAddress = REGISTER_ADDRESS_R7;
            REGISTER_INDEX_R8  : o_regAddress = REGISTER_ADDRESS_R8;
            REGISTER_INDEX_R9  : o_regAddress = REGISTER_ADDRESS_R9;
            REGISTER_INDEX_R10 : o_regAddress = REGISTER_ADDRESS_R10;
            REGISTER_INDEX_R11 : o_regAddress = REGISTER_ADDRESS_R11;
            REGISTER_INDEX_R12 : o_regAddress = REGISTER_ADDRESS_R12;
            REGISTER_INDEX_R13 : o_regAddress = REGISTER_ADDRESS_R13;
            REGISTER_INDEX_R14 : o_regAddress = REGISTER_ADDRESS_R14;
            REGISTER_INDEX_LR  : o_regAddress = REGISTER_ADDRESS_LR;
            REGISTER_INDEX_SP  : o_regAddress = REGISTER_ADDRESS_SP;
            default: o_regAddress = 32'hDEADBEEF;
        endcase
    end
endmodule
 
