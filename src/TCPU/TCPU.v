`default_nettype none   // Don't allow undeclared nets

module TCPU
#(parameter IO_CHANNEL_COUNT = 1)
(
    input wire i_clock,
    inout wire i_RSTn,
    
    output wire [(IO_CHANNEL_COUNT - 1):0] o_output
);

    // Instruction Decoder State
    reg [26:0]  r_instructionBeingExecuted;
    wire [4:0]  w_instructionOpCode;
    wire [4:0]  w_instructionArg0;
    wire [15:0] w_instructionArg1; 
    wire        w_instructionArg1IsAReg;
    wire        w_instructionIsValid;

    // Memory I/O State
    wire w_writeDataToMemory;
/* verilator lint_off UNUSED */
    wire [31:0] w_memoryDataBus;
/* verilator lint_on UNUSED */
    wire [31:0] w_memoryDataBusAddress;
    wire [31:0] w_absoluteMemoryAddress;
    wire w_romEnable;
    wire w_ramEnable;
    wire w_gpioEnable;
    wire w_absoluteMemoryAddressIsInvalid;
    wire w_absoluteMemoryAddressIsReadOnly;

    localparam WORD_SIZE_IN_BYTES = 32'h4;
    
    localparam ROM_SIZE_IN_WORDS = 32'h40;  // 256 bytes
    localparam RAM_SIZE_IN_WORDS = 32'h05; // 1024 bytes

    AddrDecoder 
    #(
        .ROM_BASE_ADDR(32'h00000000), .ROM_SIZE_IN_BYTES(ROM_SIZE_IN_WORDS * WORD_SIZE_IN_BYTES), 
        .RAM_BASE_ADDR(32'h000FFFF0), .RAM_SIZE_IN_BYTES(RAM_SIZE_IN_WORDS * WORD_SIZE_IN_BYTES),
        .GPIO_BASE_ADDR(32'hFFFFFF00), .GPIO_SIZE_IN_BYTES(((IO_CHANNEL_COUNT / 32) + 1) * WORD_SIZE_IN_BYTES)
    ) 
    addrDecoder
    (
        .i_rawAddress(w_absoluteMemoryAddress),
        .o_romEnable(w_romEnable),
        .o_ramEnable(w_ramEnable),
        .o_gpioEnable(w_gpioEnable),
        .o_decodedAddress(w_memoryDataBusAddress),
        .o_invalidAddress(w_absoluteMemoryAddressIsInvalid),
        .o_addressIsReadOnly(w_absoluteMemoryAddressIsReadOnly)
    );
    
    ROMMemory #(.MEMORY_SIZE_IN_WORDS(ROM_SIZE_IN_WORDS)) ROMMemory
    (
        .i_clock(i_clock),
        .i_RSTn(i_RSTn),
        .i_enabled(w_romEnable),
        
        .i_address(w_memoryDataBusAddress),
        .io_data(w_memoryDataBus)
    );
    
    RAMMemory #(.MEMORY_SIZE_IN_WORDS(RAM_SIZE_IN_WORDS)) RAMMemory
    (
        .i_clock(i_clock),
        .i_RSTn(i_RSTn),
        .i_enabled(w_ramEnable),
        
        .i_address(w_memoryDataBusAddress),
        .i_writing(w_writeDataToMemory),
        .io_data(w_memoryDataBus)
    );
    
    GPIOModule #(.IO_CHANNEL_COUNT(IO_CHANNEL_COUNT)) GPIOModule
    (
        .i_clock(i_clock),
        .i_RSTn(i_RSTn),
        .i_enabled(w_gpioEnable),

        .i_address(w_memoryDataBusAddress),
        .i_writing(w_writeDataToMemory),
        .io_data(w_memoryDataBus),
        .o_output(o_output)
    );
    
    localparam INSTRUCTION_OPCODE_JMP  = 5'h00;  
    localparam INSTRUCTION_OPCODE_JEQ  = 5'h01;  
    localparam INSTRUCTION_OPCODE_JNE  = 5'h02;  
    localparam INSTRUCTION_OPCODE_JGT  = 5'h03;  
    localparam INSTRUCTION_OPCODE_JLT  = 5'h04;  
    localparam INSTRUCTION_OPCODE_JGE  = 5'h05;  
    localparam INSTRUCTION_OPCODE_JLE  = 5'h06;  
    localparam INSTRUCTION_OPCODE_ADD  = 5'h07;  
    localparam INSTRUCTION_OPCODE_SUB  = 5'h08;  
    localparam INSTRUCTION_OPCODE_MUL  = 5'h09;  
    localparam INSTRUCTION_OPCODE_INC  = 5'h0A;  
    localparam INSTRUCTION_OPCODE_DEC  = 5'h0B;  
    localparam INSTRUCTION_OPCODE_XOR  = 5'h0C;  
    localparam INSTRUCTION_OPCODE_ORR  = 5'h0D;  
    localparam INSTRUCTION_OPCODE_AND  = 5'h0E;  
    localparam INSTRUCTION_OPCODE_NOT  = 5'h0F;  
    localparam INSTRUCTION_OPCODE_SHL  = 5'h10;  
    localparam INSTRUCTION_OPCODE_SHR  = 5'h11;  
    localparam INSTRUCTION_OPCODE_CMP  = 5'h12;  
    localparam INSTRUCTION_OPCODE_MOV  = 5'h13;  
    localparam INSTRUCTION_OPCODE_LDR  = 5'h14;  
    localparam INSTRUCTION_OPCODE_LDU  = 5'h15;
    localparam INSTRUCTION_OPCODE_LDL  = 5'h16;  
    localparam INSTRUCTION_OPCODE_STR  = 5'h17;
    localparam INSTRUCTION_OPCODE_PUSH = 5'h18;
    localparam INSTRUCTION_OPCODE_POP  = 5'h19;
    
    localparam MIN_OP_CODE = INSTRUCTION_OPCODE_JMP;
    localparam MAX_OP_CODE = INSTRUCTION_OPCODE_POP;

    localparam REGISTER_INDEX_R0  = 5'd0;
    localparam REGISTER_INDEX_R1  = 5'd1;
    localparam REGISTER_INDEX_R2  = 5'd2;
    localparam REGISTER_INDEX_R3  = 5'd3;
    localparam REGISTER_INDEX_R4  = 5'd4;
    localparam REGISTER_INDEX_R5  = 5'd5;
    localparam REGISTER_INDEX_R6  = 5'd6;
    localparam REGISTER_INDEX_R7  = 5'd7;
    localparam REGISTER_INDEX_R8  = 5'd8;
    localparam REGISTER_INDEX_R9  = 5'd9;
    localparam REGISTER_INDEX_R10 = 5'd10;
    localparam REGISTER_INDEX_R11 = 5'd11;
    localparam REGISTER_INDEX_R12 = 5'd12;
    localparam REGISTER_INDEX_R13 = 5'd13;
    localparam REGISTER_INDEX_R14 = 5'd14;
    localparam REGISTER_INDEX_LR  = 5'd15;
    localparam REGISTER_INDEX_SP  = 5'd16;

    localparam MIN_REG_ID = REGISTER_INDEX_R0;
    localparam MAX_REG_ID = REGISTER_INDEX_SP;
                                
    localparam REGISTER_ADDRESS_R0  = 32'b00000000000000001;
    localparam REGISTER_ADDRESS_R1  = 32'b00000000000000010;
    localparam REGISTER_ADDRESS_R2  = 32'b00000000000000100;
    localparam REGISTER_ADDRESS_R3  = 32'b00000000000001000;
    localparam REGISTER_ADDRESS_R4  = 32'b00000000000010000;
    localparam REGISTER_ADDRESS_R5  = 32'b00000000000100000;
    localparam REGISTER_ADDRESS_R6  = 32'b00000000001000000;
    localparam REGISTER_ADDRESS_R7  = 32'b00000000010000000;
    localparam REGISTER_ADDRESS_R8  = 32'b00000000100000000;
    localparam REGISTER_ADDRESS_R9  = 32'b00000001000000000;
    localparam REGISTER_ADDRESS_R10 = 32'b00000010000000000;
    localparam REGISTER_ADDRESS_R11 = 32'b00000100000000000;
    localparam REGISTER_ADDRESS_R12 = 32'b00001000000000000;
    localparam REGISTER_ADDRESS_R13 = 32'b00010000000000000;
    localparam REGISTER_ADDRESS_R14 = 32'b00100000000000000;
    localparam REGISTER_ADDRESS_LR  = 32'b01000000000000000;
    localparam REGISTER_ADDRESS_SP  = 32'b10000000000000000;
    
    InstructionDecoder
    #(
        .REGISTER_INDEX_SP(REGISTER_INDEX_SP),
        .INSTRUCTION_OPCODE_PUSH(INSTRUCTION_OPCODE_PUSH),
        .INSTRUCTION_OPCODE_POP(INSTRUCTION_OPCODE_POP),

        .MIN_OP_CODE(MIN_OP_CODE),
        .MAX_OP_CODE(MAX_OP_CODE),

        .MIN_REG_ID(MIN_REG_ID),
        .MAX_REG_ID(MAX_REG_ID)
    )
    instructionDecoder
    (
        .i_instruction(r_instructionBeingExecuted),
        
        .o_opCode(w_instructionOpCode), 
        .o_arg0(w_instructionArg0), 
        .o_arg1(w_instructionArg1), 
        .o_arg1IsAReg(w_instructionArg1IsAReg),
        .o_instructionIsValid(w_instructionIsValid)
    );

    ExecutionUnit 
    #(
        .INSTRUCTION_OPCODE_JMP(INSTRUCTION_OPCODE_JMP),  
        .INSTRUCTION_OPCODE_JEQ(INSTRUCTION_OPCODE_JEQ),  
        .INSTRUCTION_OPCODE_JNE(INSTRUCTION_OPCODE_JNE),  
        .INSTRUCTION_OPCODE_JGT(INSTRUCTION_OPCODE_JGT),  
        .INSTRUCTION_OPCODE_JLT(INSTRUCTION_OPCODE_JLT),  
        .INSTRUCTION_OPCODE_JGE(INSTRUCTION_OPCODE_JGE),  
        .INSTRUCTION_OPCODE_JLE(INSTRUCTION_OPCODE_JLE),  
        .INSTRUCTION_OPCODE_ADD(INSTRUCTION_OPCODE_ADD),  
        .INSTRUCTION_OPCODE_SUB(INSTRUCTION_OPCODE_SUB),  
        .INSTRUCTION_OPCODE_MUL(INSTRUCTION_OPCODE_MUL),  
        .INSTRUCTION_OPCODE_INC(INSTRUCTION_OPCODE_INC),  
        .INSTRUCTION_OPCODE_DEC(INSTRUCTION_OPCODE_DEC),  
        .INSTRUCTION_OPCODE_XOR(INSTRUCTION_OPCODE_XOR),  
        .INSTRUCTION_OPCODE_ORR(INSTRUCTION_OPCODE_ORR),  
        .INSTRUCTION_OPCODE_AND(INSTRUCTION_OPCODE_AND),  
        .INSTRUCTION_OPCODE_NOT(INSTRUCTION_OPCODE_NOT),  
        .INSTRUCTION_OPCODE_SHL(INSTRUCTION_OPCODE_SHL),  
        .INSTRUCTION_OPCODE_SHR(INSTRUCTION_OPCODE_SHR),  
        .INSTRUCTION_OPCODE_CMP(INSTRUCTION_OPCODE_CMP),  
        .INSTRUCTION_OPCODE_MOV(INSTRUCTION_OPCODE_MOV),  
        .INSTRUCTION_OPCODE_LDR(INSTRUCTION_OPCODE_LDR),  
        .INSTRUCTION_OPCODE_LDU(INSTRUCTION_OPCODE_LDU),  
        .INSTRUCTION_OPCODE_LDL(INSTRUCTION_OPCODE_LDL),  
        .INSTRUCTION_OPCODE_STR(INSTRUCTION_OPCODE_STR),  
        .INSTRUCTION_OPCODE_PUSH(INSTRUCTION_OPCODE_PUSH), 
        .INSTRUCTION_OPCODE_POP(INSTRUCTION_OPCODE_POP),

        .REGISTER_INDEX_R0(REGISTER_INDEX_R0),
        .REGISTER_INDEX_R1(REGISTER_INDEX_R1),
        .REGISTER_INDEX_R2(REGISTER_INDEX_R2),
        .REGISTER_INDEX_R3(REGISTER_INDEX_R3),
        .REGISTER_INDEX_R4(REGISTER_INDEX_R4),
        .REGISTER_INDEX_R5(REGISTER_INDEX_R5),
        .REGISTER_INDEX_R6(REGISTER_INDEX_R6),
        .REGISTER_INDEX_R7(REGISTER_INDEX_R7),
        .REGISTER_INDEX_R8(REGISTER_INDEX_R8),
        .REGISTER_INDEX_R9(REGISTER_INDEX_R9),
        .REGISTER_INDEX_R10(REGISTER_INDEX_R10),
        .REGISTER_INDEX_R11(REGISTER_INDEX_R11),
        .REGISTER_INDEX_R12(REGISTER_INDEX_R12),
        .REGISTER_INDEX_R13(REGISTER_INDEX_R13),
        .REGISTER_INDEX_R14(REGISTER_INDEX_R14),
        .REGISTER_INDEX_LR(REGISTER_INDEX_LR),
        .REGISTER_INDEX_SP(REGISTER_INDEX_SP),
        .REGISTER_ADDRESS_R0(REGISTER_ADDRESS_R0),
        .REGISTER_ADDRESS_R1(REGISTER_ADDRESS_R1),
        .REGISTER_ADDRESS_R2(REGISTER_ADDRESS_R2),
        .REGISTER_ADDRESS_R3(REGISTER_ADDRESS_R3),
        .REGISTER_ADDRESS_R4(REGISTER_ADDRESS_R4),
        .REGISTER_ADDRESS_R5(REGISTER_ADDRESS_R5),
        .REGISTER_ADDRESS_R6(REGISTER_ADDRESS_R6),
        .REGISTER_ADDRESS_R7(REGISTER_ADDRESS_R7),
        .REGISTER_ADDRESS_R8(REGISTER_ADDRESS_R8),
        .REGISTER_ADDRESS_R9(REGISTER_ADDRESS_R9),
        .REGISTER_ADDRESS_R10(REGISTER_ADDRESS_R10),
        .REGISTER_ADDRESS_R11(REGISTER_ADDRESS_R11),
        .REGISTER_ADDRESS_R12(REGISTER_ADDRESS_R12),
        .REGISTER_ADDRESS_R13(REGISTER_ADDRESS_R13),
        .REGISTER_ADDRESS_R14(REGISTER_ADDRESS_R14),
        .REGISTER_ADDRESS_LR(REGISTER_ADDRESS_LR),
        .REGISTER_ADDRESS_SP(REGISTER_ADDRESS_SP)
    )
    executionUnit
    (
        .i_clock(i_clock),
        .i_RSTn(i_RSTn),
        
        .i_instructionOpCode(w_instructionOpCode), 
        .i_instructionArg0(w_instructionArg0), 
        .i_instructionArg1(w_instructionArg1), 
        .i_instructionArg1IsAReg(w_instructionArg1IsAReg),
        .i_instructionIsValid(w_instructionIsValid),
        
        .i_absoluteMemoryAddressIsInvalid(w_absoluteMemoryAddressIsInvalid),
        .i_absoluteMemoryAddressIsReadOnly(w_absoluteMemoryAddressIsReadOnly),
        
        .o_instructionBeingExecuted(r_instructionBeingExecuted),

        .o_writeDataToMemory(w_writeDataToMemory),
        .o_absoluteMemoryAddress(w_absoluteMemoryAddress),
        .io_memoryDataBus(w_memoryDataBus)
    );

endmodule
