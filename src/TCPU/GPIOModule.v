module GPIOModule
#(parameter IO_CHANNEL_COUNT = 1)
(
    input wire i_clock,
    input wire i_RSTn,
    input wire i_enabled,
    
    input wire [31:0] i_address,
    input wire i_writing,
    inout wire [31:0] io_data,
    
    output wire [(IO_CHANNEL_COUNT - 1):0] o_output
);
    reg [(IO_CHANNEL_COUNT - 1):0] r_outputLevel;

    localparam OUTPUT_LEVEL_REG_ADDR = 32'd0;

    assign o_output = r_outputLevel[0];
    
    always @ (posedge i_clock or negedge i_RSTn)
    begin
        if (!i_RSTn) 
            begin
                r_outputLevel <= {{(IO_CHANNEL_COUNT - 1){1'b0}}, 1'b0};
            end
        else
            if (i_enabled)
                begin
                    if (i_writing && (i_address == OUTPUT_LEVEL_REG_ADDR))
                        r_outputLevel <= io_data[31:(32 - IO_CHANNEL_COUNT)]; 
                end
    end

    assign io_data = (((i_writing == 1'b0) && (i_enabled == 1'b1) && (i_address == OUTPUT_LEVEL_REG_ADDR)) ? {r_outputLevel, {(32 - IO_CHANNEL_COUNT){1'b0}}} : 32'hZZZZ_ZZZZ);


endmodule
