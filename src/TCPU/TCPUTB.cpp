#include "TCPUTB.hpp"
#include <iostream>


enum class ClockID : uint32_t 
{
    Default_25kHz = 0
};

enum class ALUOp : uint8_t 
{
    Add = 0,
    Inc = 1,
    Sub = 2,
    Dec = 3,
    Mul = 4
};


TCPUTB::TCPUTB()
: TestBench<UT_VERILOG_MODULE_TO_TEST>(FreqUnit::kHz, 25, mainClockTick)
{
}

TCPUTB::~TCPUTB() 
{

}

#ifdef UT_VERILOG_TEST_BENCH_IS_CONTINUOUS
void TCPUTB::setupBeforeContinuousTest()
{

}
#else
// static uint64_t ALUOpWidthInBits = 64;
// 
// static uint64_t truncateResult(uint64_t result)
// {
//     return (result & (0xFFFFFFFFFFFFFFFFull >> (64 - ALUOpWidthInBits)));
// }
// 
// bool TCPUTB::_additionTest(uint64_t a, uint64_t b)
// {
//     auto& core = verilogModule();
//     
//     core.i_operand0 = a;
//     core.i_operand1 = b;
//     core.i_operation = static_cast<uint8_t>(ALUOp::Add);
//     waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));
// 
//     return (core.o_result == truncateResult(a + b));
// }
// 
// bool TCPUTB::_subtractionTest(uint64_t a, uint64_t b)
// {
//     auto& core = verilogModule();
//     
//     core.i_operand0 = a;
//     core.i_operand1 = b;
//     core.i_operation = static_cast<uint8_t>(ALUOp::Sub);
//     waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));
//     
//     return (core.o_result == truncateResult(a - b));
// }
// 
// bool TCPUTB::_incrementTest(uint64_t a)
// {
//     auto& core = verilogModule();
//     
//     core.i_operand0 = a;
//     core.i_operand1 = 0xFF;
//     core.i_operation = static_cast<uint8_t>(ALUOp::Inc);
//     waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));
// 
//     return (core.o_result == truncateResult(a + 1));
// }
// 
// bool TCPUTB::_decrementTest(uint64_t a)
// {
//     auto& core = verilogModule();
//     
//     core.i_operand0 = a;
//     core.i_operand1 = 0xFF;
//     core.i_operation = static_cast<uint8_t>(ALUOp::Dec);
//     waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));
// 
//     return (core.o_result == truncateResult(a - 1));
// }
// 
// bool TCPUTB::_multiplicationTest(uint64_t a, uint64_t b)
// {
//     auto& core = verilogModule();
//     
//     core.i_operand0 = a;
//     core.i_operand1 = b;
//     core.i_operation = static_cast<uint8_t>(ALUOp::Mul);
//     waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));
// 
//     return (core.o_result == truncateResult(a * b));
// }

void TCPUTB::test() 
{     
    // Reset verilog logic
    reset(TimeUnit::us, 1);
    
    waitForNTicks(20000, static_cast<uint32_t>(ClockID::Default_25kHz));

//     std::cout << "Addition:       " << _additionTest(0xFFFFFFFFFFFFFFFF, 1) << std::endl;
//     std::cout << "Subtraction:    " << _subtractionTest(1, 2) << std::endl;
//     std::cout << "Increment:      " << _incrementTest(1) << std::endl;
//     std::cout << "Decrement:      " << _decrementTest(1) << std::endl;
//     std::cout << "Multiplication: " << _multiplicationTest(0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF) << std::endl;


    Verilated::gotFinish(true);  
}
#endif

void TCPUTB::mainClockTick(TestBench<UT_VERILOG_MODULE_TO_TEST>* UNUSED_PARAM(thisPtr))
{
    
}
