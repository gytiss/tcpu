`default_nettype none   // Don't allow undeclared nets

module AddrDecoder
#(
    parameter ROM_BASE_ADDR = 32'h00000000, parameter ROM_SIZE_IN_BYTES = 32'h100, 
    parameter RAM_BASE_ADDR = 32'h000FFFF0, parameter RAM_SIZE_IN_BYTES = 32'h100,
    parameter GPIO_BASE_ADDR = 32'hFFFFFF00, parameter GPIO_SIZE_IN_BYTES = 32'h100
)
(
    input wire [31:0] i_rawAddress,
    output wire o_romEnable,
    output wire o_ramEnable,
    output wire o_gpioEnable,
    output wire [31:0] o_decodedAddress,
    output wire o_invalidAddress,
    output wire o_addressIsReadOnly
);
    
/* verilator lint_off UNSIGNED */
    assign o_romEnable = ((i_rawAddress >= ROM_BASE_ADDR) && ((i_rawAddress - ROM_BASE_ADDR) < (ROM_BASE_ADDR + ROM_SIZE_IN_BYTES)));
/* verilator lint_on UNSIGNED */

    assign o_ramEnable = ((i_rawAddress >= RAM_BASE_ADDR) && ((i_rawAddress - RAM_BASE_ADDR) < (RAM_BASE_ADDR + RAM_SIZE_IN_BYTES)));
    
    assign o_gpioEnable = ((i_rawAddress >= GPIO_BASE_ADDR) && ((i_rawAddress - GPIO_BASE_ADDR) < (GPIO_BASE_ADDR + GPIO_SIZE_IN_BYTES)));

    assign o_decodedAddress = (o_romEnable ? (i_rawAddress - ROM_BASE_ADDR) : o_ramEnable ? (i_rawAddress - RAM_BASE_ADDR) : o_gpioEnable ? (i_rawAddress - GPIO_BASE_ADDR) : 32'h00000000);
    
    assign o_invalidAddress = ((o_romEnable == 1'b0) && (o_ramEnable == 1'b0) && (o_gpioEnable == 1'b0));

    assign o_addressIsReadOnly = (o_romEnable);
endmodule
