#ifndef CRC_TB_HPP_INCLUDED 
#define CRC_TB_HPP_INCLUDED

#include "VerilatorHelpers/TestBench.hpp"
#include "VerilatorHelpers/TopVerilogModuleInclude.hpp"

class TCPUTB : public TestBench<UT_VERILOG_MODULE_TO_TEST>
{
public:
    TCPUTB();
    
    virtual ~TCPUTB();

#ifdef UT_VERILOG_TEST_BENCH_IS_CONTINUOUS
    virtual void setupBeforeContinuousTest() override;
#else
    virtual void test() override;
#endif
    
private:
//     bool _additionTest(uint64_t a, uint64_t b);
//     bool _subtractionTest(uint64_t a, uint64_t b);
//     bool _incrementTest(uint64_t a);
//     bool _decrementTest(uint64_t a);
//     bool _multiplicationTest(uint64_t a, uint64_t b);
    
    static void mainClockTick(TestBench<UT_VERILOG_MODULE_TO_TEST>* thisPtr);
};

#endif // CRC_TB_HPP_INCLUDED
 
