`default_nettype none   // Don't allow undeclared nets

module TCPU_Top_FPGA
#(parameter IO_CHANNEL_COUNT = 1)
( 
    input wire i_clock,
    //input wire i_asyncRSTn,
    output wire [(IO_CHANNEL_COUNT - 1):0] o_output,
    

    output wire LED1,
    output wire LED2,
    output wire LED3,
    output wire LED4,
    output wire LED5,
    output wire LED6,
    output wire LED7
);
/* verilator lint_off UNUSED */
    wire w_rstN;
/* verilator lint_on UNUSED */

    reg i_asyncRSTn;
    reg [31:0] r_resetCounter;
    
    initial
    begin
        i_asyncRSTn = 1'b0;
        r_resetCounter = 32'b0;
    end
    

    assign LED1 = 1'b1;
    //assign LED2 = 1'b0;
    assign LED3 = 1'b0;
    assign LED4 = 1'b0;
    assign LED5 = 1'b0;
    assign LED6 = 1'b0;
    assign LED7 = 1'b0;

    AsyncResetSynchronizer AsyncRstSync
    (
        .i_clock(i_clock),
        .i_asyncRSTn(i_asyncRSTn),
        .o_RSTn(w_rstN)
    );

    TCPU tcpu
    (
        .i_clock(i_clock),
        .i_RSTn(w_rstN),
        .o_output(o_output)
    );
    

    
    always @ (posedge i_clock)
    begin
        if (r_resetCounter < 32'd24000000)
            begin
                r_resetCounter = r_resetCounter + 32'd1;
            end
        else
            begin
                i_asyncRSTn <= 1'b1;
            end
    end
    
    assign LED2 = i_asyncRSTn;

endmodule 
 
