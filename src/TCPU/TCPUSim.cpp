#include <iostream>
#include <vector>
#include <cstdint>
#include <array>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <iomanip>

class StrUtils
{
public:
    template <class T>
    static std::string numToHex(T num)
    {
        std::stringstream stream;
        stream << std::hex << std::setfill('0') << std::setw(8) << static_cast<uint64_t>(num);
        std::string result(stream.str());
        return result;
    }
};

class Memory
{
public:
    Memory & operator=(const Memory&) = delete;
    Memory(const Memory&) = delete;
    Memory() = default;
    
    Memory(size_t memSizeInBytes, bool isReadOnly)
    : m_isReadOnly(isReadOnly),
      m_data(memSizeInBytes, 0xFFu)
    {
        
    }
    
    bool isReadOnly() const
    {
        return m_isReadOnly;
    }
    
    uint32_t read(size_t addr) const
    {
        const uint8_t* data = (m_data.data() + addr);
        return ((static_cast<uint32_t>(data[3]) << 24u) | (static_cast<uint32_t>(data[2]) << 16u) | (static_cast<uint32_t>(data[1]) << 8u) | static_cast<uint32_t>(data[0]));
    }
    
    void write(size_t addr, uint32_t val)
    {
        uint8_t* data = (m_data.data() + addr);
        data[3] = ((val >> 24u) & 0xFFu); 
        data[2] = ((val >> 16u) & 0xFFu);
        data[1] = ((val >> 8u) & 0xFFu) ;
        data[0] = (val & 0xFFu);
    }
    
    uint32_t size() const
    {
        return m_data.size();
    }
    
    void saveAsVerilogArray(const std::string& outFilePath)
    {
        try
        {
            std::fstream file;
            file.open(outFilePath.c_str(), std::ios::out);

            file << "reg [31:0] a_ROM [(MEMORY_SIZE_IN_WORDS - 1):0];" << std::endl;
            
            file << "initial begin" << std::endl;
            
            const uint32_t dataSizeInWords = (size() / 4ul);
            for  (size_t i = 0; i < dataSizeInWords; i++)
            {
                file << "    a_ROM[" + std::to_string(i) + "] = 32'h" << StrUtils::numToHex(read(i * 4ul)) << ";" << std::endl;
            }
            file << "end" << std::endl;
        }
        catch (const std::ifstream::failure& e) 
        {
            std::cout << "Failed to write to \"" << outFilePath << "\"" << std::endl;
        }
    }
    
    void saveAsCArray(const std::string& outFilePath)
    {
        try
        {
            std::fstream file;
            file.open(outFilePath.c_str(), std::ios::out);
            
            file << "#include <cstdint>" << std::endl;
            file << "uint32_t a_ROM [" << size() << "] =" << std::endl;
            
            file << "{" << std::endl;
            
            const uint32_t dataSize = size();
            for  (size_t i = 0; i < dataSize; i++)
            {
                file << "    0x" << StrUtils::numToHex(read(i * 4ul)) << "," << std::endl;
            }
            file << "};" << std::endl;
        }
        catch (const std::ifstream::failure& e) 
        {
            std::cout << "Failed to write to \"" << outFilePath << "\"" << std::endl;
        }
    }
    
private:
    const bool m_isReadOnly;
    std::vector<uint8_t> m_data;
};

enum class Reg
{
    R0 = 0,
    R1 = 1,
    R2 = 2,
    R3 = 3,
    R4 = 4,
    R5 = 5,
    R6 = 6,
    R7 = 7,
    R8 = 8,
    R9 = 9,
    R10 = 10,
    R11 = 11,
    R12 = 12,
    R13 = 13,
    R14 = 14,
    LR = 15,
    SP = 16,
    Temp = 17,
    PC = 18
};

class RegisterFile
{
public:   
    RegisterFile & operator=(const RegisterFile&) = delete;
    RegisterFile(const RegisterFile&) = delete;
    
    constexpr static uint32_t RegR0Addr   = 0b0000000000000000001ul;
    constexpr static uint32_t RegR1Addr   = 0b0000000000000000010ul;
    constexpr static uint32_t RegR2Addr   = 0b0000000000000000100ul;
    constexpr static uint32_t RegR3Addr   = 0b0000000000000001000ul;
    constexpr static uint32_t RegR4Addr   = 0b0000000000000010000ul;
    constexpr static uint32_t RegR5Addr   = 0b0000000000000100000ul;
    constexpr static uint32_t RegR6Addr   = 0b0000000000001000000ul;
    constexpr static uint32_t RegR7Addr   = 0b0000000000010000000ul;
    constexpr static uint32_t RegR8Addr   = 0b0000000000100000000ul;
    constexpr static uint32_t RegR9Addr   = 0b0000000001000000000ul;
    constexpr static uint32_t RegR10Addr  = 0b0000000010000000000ul;
    constexpr static uint32_t RegR11Addr  = 0b0000000100000000000ul;
    constexpr static uint32_t RegR12Addr  = 0b0000001000000000000ul;
    constexpr static uint32_t RegR13Addr  = 0b0000010000000000000ul;
    constexpr static uint32_t RegR14Addr  = 0b0000100000000000000ul;
    constexpr static uint32_t RegLRAddr   = 0b0001000000000000000ul;
    constexpr static uint32_t RegSPAddr   = 0b0010000000000000000ul;
    constexpr static uint32_t RegTempAddr = 0b0100000000000000000ul;
    constexpr static uint32_t RegPCAddr   = 0b1000000000000000000ul;
    
    RegisterFile()
    : m_regs(),
      m_memStoreQueued(false),
      m_memLoadQueued(false),
      m_quedMemStoreReg0Addr(0),
      m_quedMemStoreReg1Addr(0),
      m_quedMemLoadReg0Addr(0),
      m_quedMemLoadReg1Addr(0),
      m_Flags(0)
    {
    }
    
    enum class Flag
    {
        CF = 0, // ALU op resulted in a carry
        ZF = 1, // ALU operation result is zero
        OF = 2, // ALU op resulted in an overflow (negative or positive)
        NF = 3  // ALU op operation resulted in a negative number
    };
    
    bool flag(Flag f)
    {
        return ((m_Flags & (1 << static_cast<uint8_t>(f))) == (1 << static_cast<uint8_t>(f))); 
    }

    void setFlag(Flag f, bool val)
    {
        if (val)
        {
            m_Flags |= (1 << static_cast<uint8_t>(f));
        }
        else
        {
            m_Flags &= ~(1 << static_cast<uint8_t>(f));
        }
    }
    
    uint32_t read(uint32_t regAddr) const
    {
        const uint32_t retVal = *_getAddrToRegPtrConst(regAddr);
        
        std::cout << "Reading reg addr: 0x" << std::hex << regAddr << ", " << retVal << std::endl;
        
        return retVal;
    }
    
    void write(uint32_t regAddr, uint32_t regVal)
    {
        std::cout << "Writing reg addr: 0x" << std::hex << regAddr << ", " << regVal << std::endl;
        
        *_getAddrToRegPtr(regAddr) = regVal;
    }
    
    uint32_t readPC() const
    {
        const uint32_t retVal = m_regs[static_cast<uint32_t>(Reg::PC)];
        std::cout << "Reading PC register: 0x" << std::hex << retVal << std::endl;
        return retVal;
    }
    
    void writePC(uint32_t regVal)
    {
        std::cout << "Writing PC register: 0x" << std::hex << regVal << std::endl;
        m_regs[static_cast<uint32_t>(Reg::PC)] = regVal;
    }
    
    void queueMemStore(uint32_t regAddr0, uint32_t regAddr1)
    {
        std::cout << "Queing a store" << std::endl;
        
        m_memStoreQueued = true;
        m_quedMemStoreReg0Addr = regAddr0;
        m_quedMemStoreReg1Addr = regAddr1;
    }
    
    void queueMemLoad(uint32_t regAddr0, uint32_t regAddr1)
    {
        std::cout << "Queing a load" << std::endl;
        
        m_memLoadQueued = true;
        m_quedMemLoadReg0Addr = regAddr0;
        m_quedMemLoadReg1Addr = regAddr1;
    }
    
    bool memStoreQueued() const
    {
        return m_memStoreQueued;
    }
    
    bool memLoadQueued() const
    {
        return m_memLoadQueued;
    }
    
    uint32_t queuedStoreReg0() const
    {
        return m_quedMemStoreReg0Addr;
    }
    
    uint32_t queuedStoreReg1() const
    {
        return m_quedMemStoreReg1Addr;
    }
    
    uint32_t queuedLoadReg0() const
    {
        return m_quedMemLoadReg0Addr;
    }
    
    uint32_t queuedLoadReg1() const
    {
        return m_quedMemLoadReg1Addr;
    }
    
    void markMemStoreAsDone()
    {
        m_memStoreQueued = false;
    }
    
    void markMemLoadAsDone()
    {
        m_memLoadQueued = false;
    }
    
private:
    const uint32_t* _getAddrToRegPtrConst(uint32_t regAddr) const
    {
        const uint32_t* retVal = m_regs.data();
        
        switch(regAddr)
        {
            case RegR0Addr:   retVal = (retVal + 0); break;
            case RegR1Addr:   retVal = (retVal + 1); break;
            case RegR2Addr:   retVal = (retVal + 2); break;
            case RegR3Addr:   retVal = (retVal + 3); break;
            case RegR4Addr:   retVal = (retVal + 4); break;
            case RegR5Addr:   retVal = (retVal + 5); break;
            case RegR6Addr:   retVal = (retVal + 6); break;
            case RegR7Addr:   retVal = (retVal + 7); break;
            case RegR8Addr:   retVal = (retVal + 8); break;
            case RegR9Addr:   retVal = (retVal + 9); break;
            case RegR10Addr:  retVal = (retVal + 10); break;
            case RegR11Addr:  retVal = (retVal + 11); break;
            case RegR12Addr:  retVal = (retVal + 12); break;
            case RegR13Addr:  retVal = (retVal + 13); break;
            case RegR14Addr:  retVal = (retVal + 14); break;
            case RegLRAddr:   retVal = (retVal + 15); break;
            case RegSPAddr:   retVal = (retVal + 16); break;
            case RegTempAddr: retVal = (retVal + 17); break;
            case RegPCAddr:   retVal = (retVal + 18); break;
            default: retVal = nullptr; break;
        }
        return retVal;
    }
    
    uint32_t* _getAddrToRegPtr(uint32_t regAddr)
    {
        const uint32_t* retVal =  _getAddrToRegPtrConst(regAddr);
        return const_cast<uint32_t*>(retVal);
    }

    std::array<uint32_t, static_cast<uint32_t>(Reg::PC) + 1> m_regs;
    
    bool m_memStoreQueued;
    bool m_memLoadQueued;
    uint32_t m_quedMemStoreReg0Addr;
    uint32_t m_quedMemStoreReg1Addr;
    uint32_t m_quedMemLoadReg0Addr;
    uint32_t m_quedMemLoadReg1Addr;
    uint8_t m_Flags;
};

class RegAddrDec
{
public:
    static bool regIDToRegAddr(uint32_t regID, uint32_t& outRegAddr)
    {
        bool retVal = false;

        for(const auto& rec : m_regIDToRegAddrMap)
        {
            if  (static_cast<uint32_t>(rec.regID) == regID)
            {
                outRegAddr = rec.regAddr;
                retVal = true;
                break;
            }
        }
        
        return retVal;
    }

private:
    struct RegIDToRegAddrMap
    {
        Reg regID;
        uint32_t regAddr;
    };
    
    static RegIDToRegAddrMap m_regIDToRegAddrMap[static_cast<uint32_t>(Reg::PC) + 1];
};

RegAddrDec::RegIDToRegAddrMap RegAddrDec::m_regIDToRegAddrMap[static_cast<uint32_t>(Reg::PC) + 1] =
{
    { Reg::R0,   RegisterFile::RegR0Addr   },
    { Reg::R1,   RegisterFile::RegR1Addr   },
    { Reg::R2,   RegisterFile::RegR2Addr   },
    { Reg::R3,   RegisterFile::RegR3Addr   },
    { Reg::R4,   RegisterFile::RegR4Addr   },
    { Reg::R5,   RegisterFile::RegR5Addr   },
    { Reg::R6,   RegisterFile::RegR6Addr   },
    { Reg::R7,   RegisterFile::RegR7Addr   },
    { Reg::R8,   RegisterFile::RegR8Addr   },
    { Reg::R9,   RegisterFile::RegR9Addr   },
    { Reg::R10,  RegisterFile::RegR10Addr  },
    { Reg::R11,  RegisterFile::RegR11Addr  },
    { Reg::R12,  RegisterFile::RegR12Addr  },
    { Reg::R13,  RegisterFile::RegR13Addr  },
    { Reg::R14,  RegisterFile::RegR14Addr  },
    { Reg::LR,   RegisterFile::RegLRAddr   },
    { Reg::SP,   RegisterFile::RegSPAddr   },
    { Reg::Temp, RegisterFile::RegTempAddr },
    { Reg::PC,   RegisterFile::RegPCAddr   }
};

enum class OpCode
{
    jmp = 0x00,
    jeq = 0x01,
    jne = 0x02,
    jgt = 0x03,        
    jlt = 0x04,
    jge = 0x05,
    jle = 0x06,
    add = 0x07,
    sub = 0x08,
    mul = 0x09,
    inc = 0x0A,
    dec = 0x0B,
    Xor = 0x0C,
    orr = 0x0D,
    And = 0x0E,
    Not = 0x0F,
    shl = 0x10,
    shr = 0x11,
    cmp = 0x12,    
    mov = 0x13,
    ldr = 0x14,
    ldu = 0x15,
    ldl = 0x16,
    str = 0x17,
    push = 0x18,
    pop = 0x19
};

class InstDec
{
public:
    static bool decode(uint32_t instruction, uint16_t& outOpCode, uint32_t& outArg0, uint32_t& outArg1, bool& outArg1IsReg)
    {
        bool validInstruction = false;
        
        const uint16_t opCode = ((instruction >> 27u) & 0x1Fu);
        const uint16_t arg0 = ((instruction >> 22u) & 0x1Fu);
        const uint16_t arg1 = ((instruction >> 5u) & 0xFFFFu);
        const uint16_t arg1IRegister = ((instruction >> 21u) & 0x01u);
        
        std::cout << "Decoded OpCode, arg0, arg1, arg1IRegister: 0x" << std::hex << opCode << ", 0x" << arg0 << ", 0x" << arg1 << ", 0x" << arg1IRegister << std::endl;
        
        if ((opCode >= MinOpCode) && (opCode <= MaxOpCode))
        {
            uint32_t arg0RegAddr = 0xFFFFFFFFu;

            if (RegAddrDec::regIDToRegAddr(arg0, arg0RegAddr))
            {

                
//TODO
//TODO
//TODO
//TODO
//TODO
//                 if (OpCode and args combination is fine)
//                 {
                    if (arg1IRegister == true)
                    {
                        uint32_t arg1RegAddr = 0xFFFFFFFFu;
                        if (RegAddrDec::regIDToRegAddr(arg1, arg1RegAddr))
                        {
                            outArg1IsReg = arg1IRegister;       
                            outOpCode = opCode;
                            outArg0 = arg0RegAddr;
                            outArg1 = arg1RegAddr;
                            validInstruction = true;
                        }
                    }
                    else
                    {
                        validInstruction = true;
                        outArg1IsReg = arg1IRegister;       
                        outOpCode = opCode;
                        outArg0 = arg0RegAddr;
                        outArg1 = arg1;
                    }
                    
//                 }
            }
        }

        return validInstruction;
    }
    
private:
    constexpr static uint8_t MinOpCode = 0x00u;
    constexpr static uint8_t MaxOpCode = 0x19u;
};

class MemAddrDecode
{
public:
    virtual bool read(uint32_t memAddr, uint32_t& outReadVal) = 0;
    virtual bool write(uint32_t memAddr, uint32_t valToWrite) = 0;
};

template<uint32_t ROMBaseAddr, uint32_t RAMBaseAddr>
class MemAddrDecodeRAMROM : public MemAddrDecode
{
public:
    MemAddrDecodeRAMROM & operator=(const MemAddrDecodeRAMROM&) = delete;
    MemAddrDecodeRAMROM(const MemAddrDecodeRAMROM&) = delete;
    MemAddrDecodeRAMROM() = default;
    
    MemAddrDecodeRAMROM(Memory& rom, Memory& ram)
    : m_ROM(rom),
      m_RAM(ram)
    {
    }

    bool read(uint32_t memAddr, uint32_t& outReadVal) override
    {
        bool retVal = false;
        
        Memory* mem = nullptr;
        uint32_t baseAddr = 0;
        
        mem = _getMemAndBaseAddr(memAddr, baseAddr);
        
        if (mem)
        {
            outReadVal = mem->read((memAddr - baseAddr));
            retVal = true;
        }
        
        std::cout << "Mem Reading Status and val: " << retVal << ", " << std::hex << "0x" << outReadVal << std::endl;

        return retVal;
    }
    
    bool write(uint32_t memAddr, uint32_t valToWrite) override
    {
        bool retVal = false;
        
        Memory* mem = nullptr;
        uint32_t baseAddr = 0;
        
        mem = _getMemAndBaseAddr(memAddr, baseAddr);
        
        if (mem && !mem->isReadOnly())
        {
            mem->write((memAddr - baseAddr), valToWrite);
            retVal = true;
        }
        
        std::cout << "Mem Writing Status: " << retVal << ", " << std::hex << "0x" << valToWrite << std::endl;

        return retVal;
    }
    
private:    
    Memory* _getMemAndBaseAddr(uint32_t inAddr, uint32_t& outBaseAddr)
    {
        Memory* mem = nullptr;
        
        std::cout << "Mem addr to decode: 0x" << std::hex << inAddr << std::endl;
        std::cout << "ROM Base: " << std::hex << ROMBaseAddr << std::endl;
        std::cout << "RAM Base: " << std::hex << RAMBaseAddr << std::endl;
        
        if ((inAddr >= ROMBaseAddr) && ((inAddr - ROMBaseAddr) < m_ROM.size()))
        {
            mem = &m_ROM;
            outBaseAddr = ROMBaseAddr;
            
            std::cout << "ROM";
        }
        else if ((inAddr >= RAMBaseAddr) && ((inAddr - RAMBaseAddr) < m_RAM.size()))
        {
            mem = &m_RAM;
            outBaseAddr = RAMBaseAddr;
            
            std::cout << "RAM";
        }
        
        std::cout << " mem. selected with base addr. 0x" << std::hex << outBaseAddr << std::endl;
        
        return mem;
    }
    
    Memory& m_ROM;
    Memory& m_RAM;
};


class ExceptionLogic
{
public:
    
    
    static void raiseInvalidInstruction(RegisterFile& regFile)
    {
        std::cout << "EXCEPTION: Invalid Instruction." << std::endl;
        
        regFile.writePC(InvalidInstructionExceptionVectorAddr);
    }
    
    static void raiseInvalidAddr(RegisterFile& regFile)
    {
        std::cout << "EXCEPTION: Invalid Address" << std::endl;
        
        regFile.writePC(InvalidAddressExceptionVectorAddr);
    }
    
private:
    constexpr static uint32_t InvalidInstructionExceptionVectorAddr = 0x00000004u;
    constexpr static uint32_t InvalidAddressExceptionVectorAddr = 0x00000008u;
};


class InstFetch
{
public:
    static bool fetch(RegisterFile& regFile, MemAddrDecode& memDec, uint32_t& outInstruction)
    {        
        const uint32_t nextInstructionAddr = regFile.readPC();
        std::cout << "Next inst. addr: 0x" << std::hex << nextInstructionAddr << std::endl;
        return memDec.read(nextInstructionAddr, outInstruction);
    }
};

class ALU
{
public:
    enum class Op
    {
        Add,
        Sub,
        Mul,
        Inc,
        Dec,
        XOr,
        Or,
        And,
        Not
    };
    
    static uint32_t calc(Op op, uint32_t v1, uint32_t v2, bool& outCarry, bool& outResultIsZero, bool& outOverflow, bool& outNegative)
    {
        uint32_t res = 0u;
        
        outCarry = false;
        outResultIsZero = false;
        outOverflow = false;

        switch(op)
        {
            case Op::Add: res = v1 + v2; _SetFlags(op, v1, v2, outCarry, outResultIsZero, outOverflow, outNegative); break;
            case Op::Sub: res = v1 - v2; _SetFlags(op, v1, v2, outCarry, outResultIsZero, outOverflow, outNegative); break;
            case Op::Inc: res = v1 + 1;  _SetFlags(op, v1, v2, outCarry, outResultIsZero, outOverflow, outNegative); break;
            case Op::Dec: res = v1 - 1;  _SetFlags(op, v1, v2, outCarry, outResultIsZero, outOverflow, outNegative); break;
            case Op::Mul: res = v1 * v2; _SetFlags(op, v1, v2, outCarry, outResultIsZero, outOverflow, outNegative); break;
            case Op::XOr: res = v1 ^ v2;  break;
            case Op::Or:  res = v1 | v2;  break;
            case Op::And: res = v1 & v2;  break;
            case Op::Not: res = ~v1;      break;
        }

        return res;
    }
    
private:
    static void _SetFlags(Op op, uint64_t v1, uint64_t v2, bool& outCarry, bool& outResultIsZero, bool& outOverflow, bool& outNegative)
    {
        //const int32_t minInt = 0x80000000l;
        const uint32_t maxInt = 0xFFFFFFFFul;

        if  (op == Op::Inc)
        {
            op = Op::Add;
            v2 = 1ull;
        }
        else if (op == Op::Dec)
        {
            op = Op::Sub;
            v2 = 1ull;
        }

        switch(op)
        {
            case Op::Add: 
            {
                const uint64_t res = (v1 + v2);
                
                if (res > maxInt)
                {
                    outCarry = true;
                }

                if  (((v1 > 0) && (res < 0)) || ((v1 < 0) && (res > 0)))
                {
                    outOverflow = true;
                }
                
                if (res == 0ull)
                {
                    outResultIsZero = true;
                }
                
                if (res < 0ll)
                {
                    outNegative = true;
                }

                break;
            }
            
            case Op::Sub: 
            {
                const int32_t res = static_cast<int32_t>(v1) - static_cast<int32_t>(v2); 

                if  (((v1 > 0) && (res < 0)) || ((v1 < 0) && (res > 0)))
                {
                    outOverflow = true;
                }

                if (res == 0ull)
                {
                    outResultIsZero = true;
                }
                
                if (res < 0ll)
                {
                    outNegative = true;
                }
                
                break;
            }

            case Op::Mul: 
            {
                const uint64_t res = (v1 * v2);
                if (res > maxInt)
                {
                    outCarry = true;
                }
                else if (res == 0ull)
                {
                    outResultIsZero = true;
                }

                break;
            }

            case Op::Inc:
            case Op::Dec:
            default: break;
        }
    }    
};

class BarrelShifter
{
public:
    static uint32_t shiftLeft(uint32_t val, uint32_t count)
    {
        return (val << count);
    }
    
    static uint32_t shiftRight(uint32_t val, uint32_t count)
    {
        return (val >> count);
    }
    
private:
    
};

#define instDebug(x) std::cout << "Executing: " << x << std::endl

class ExecuteInstruction
{
public:
    static bool execute(uint16_t opCode, uint32_t arg0, uint32_t arg1, bool arg1IsReg, RegisterFile& regFile)
    {
        bool incrementPC = true;
        
        std::cout << "arg1IsReg: " << arg1IsReg << std::endl;
        
        bool carry = false;
        bool resultIsZero = false;
        bool overflow = false;
        bool negative = false;
        
        bool updateFlags = false;
        
        switch (static_cast<OpCode>(opCode))
        {
            case OpCode::jmp: instDebug("jmp");
            {
                if (arg1IsReg)
                {
                    std::cout << "  to address in reg: " << std::hex << arg0 << std::endl;
                    regFile.writePC(regFile.read(arg0)); 
                }
                else
                {
                    std::cout << "  to address: " << std::hex << arg1 << std::endl;
                    regFile.writePC(arg1); 
                }
                
                incrementPC = false;
                
                break;
            }

            case OpCode::jeq: instDebug("jeq"); if (regFile.flag(RegisterFile::Flag::ZF) == true) { regFile.writePC(arg1IsReg ? regFile.read(arg0) : arg1); incrementPC = false; } break;
            case OpCode::jne: instDebug("jne"); if (regFile.flag(RegisterFile::Flag::ZF) == false) { regFile.writePC(arg1IsReg ? regFile.read(arg0) : arg1); incrementPC = false; } break;
            case OpCode::jgt: instDebug("jgt"); if (regFile.flag(RegisterFile::Flag::NF) == false) { regFile.writePC(arg1IsReg ? regFile.read(arg0) : arg1); incrementPC = false; } break;
            case OpCode::jlt: instDebug("jlt"); if (regFile.flag(RegisterFile::Flag::NF) == true) { regFile.writePC(arg1IsReg ? regFile.read(arg0) : arg1); incrementPC = false; } break;
            
            case OpCode::jge: instDebug("jge"); if ((regFile.flag(RegisterFile::Flag::NF) == false) || (regFile.flag(RegisterFile::Flag::ZF) == true)) { regFile.writePC(arg1IsReg ? regFile.read(arg0) : arg1); incrementPC = false; } break;
            case OpCode::jle: instDebug("jle"); if ((regFile.flag(RegisterFile::Flag::NF) == true) || (regFile.flag(RegisterFile::Flag::ZF) == true)) { regFile.writePC(arg1IsReg ? regFile.read(arg0) : arg1); incrementPC = false; } break;
            
            case OpCode::add: instDebug("add"); regFile.write(arg0, ALU::calc(ALU::Op::Add, regFile.read(arg0), regFile.read(arg1), carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::sub: instDebug("sub"); regFile.write(arg0, ALU::calc(ALU::Op::Sub, regFile.read(arg0), regFile.read(arg1), carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::mul: instDebug("mul"); regFile.write(arg0, ALU::calc(ALU::Op::Mul, regFile.read(arg0), regFile.read(arg1), carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::inc: instDebug("inc"); regFile.write(arg0, ALU::calc(ALU::Op::Inc, regFile.read(arg0), 0, carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::dec: instDebug("dec"); regFile.write(arg0, ALU::calc(ALU::Op::Dec, regFile.read(arg0), 0, carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::Xor: instDebug("Xor"); regFile.write(arg0, ALU::calc(ALU::Op::XOr, regFile.read(arg0), regFile.read(arg1), carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::orr: instDebug("orr"); regFile.write(arg0, ALU::calc(ALU::Op::Or,  regFile.read(arg0), regFile.read(arg1), carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::And: instDebug("And"); regFile.write(arg0, ALU::calc(ALU::Op::And, regFile.read(arg0), regFile.read(arg1), carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::Not: instDebug("Not"); regFile.write(arg0, ALU::calc(ALU::Op::Not, regFile.read(arg0), 0, carry, resultIsZero, overflow, negative)); updateFlags = true; break;
            case OpCode::shl: instDebug("shl"); regFile.write(arg0, BarrelShifter::shiftLeft(regFile.read(arg0), (arg1IsReg ? regFile.read(arg1) : arg1))); break;
            case OpCode::shr: instDebug("shr"); regFile.write(arg0, BarrelShifter::shiftRight(regFile.read(arg0), (arg1IsReg ? regFile.read(arg1) : arg1))); break;
            case OpCode::cmp: instDebug("cmp"); ALU::calc(ALU::Op::Sub, regFile.read(arg0), regFile.read(arg1), carry, resultIsZero, overflow, negative); updateFlags = true; break;
            case OpCode::mov: instDebug("mov"); regFile.write(arg0, regFile.read(arg1)); break;
            case OpCode::ldr: instDebug("ldr"); regFile.queueMemLoad(arg0, arg1); break;
            case OpCode::ldu: instDebug("ldu"); regFile.write(arg0, (regFile.read(arg0) & 0x0000FFFFu) | ((arg1 << 16) & 0xFFFF0000u)); break;
            case OpCode::ldl: instDebug("ldl"); regFile.write(arg0, (regFile.read(arg0) & 0xFFFF0000u) | (arg1 & 0x0000FFFFu)); break;
            case OpCode::str: instDebug("str"); regFile.queueMemStore(arg0, arg1); break;
            case OpCode::push: instDebug("push"); regFile.write(RegisterFile::RegSPAddr, (regFile.read(RegisterFile::RegSPAddr) - 4u)); regFile.queueMemStore(arg0, RegisterFile::RegSPAddr); break;
            case OpCode::pop: instDebug("pop"); regFile.write(RegisterFile::RegTempAddr, regFile.read(RegisterFile::RegSPAddr)); regFile.queueMemLoad(arg0, RegisterFile::RegTempAddr); regFile.write(RegisterFile::RegSPAddr, (regFile.read(RegisterFile::RegSPAddr) + 4u)); break;

            default: break;
        }

        if (updateFlags)
        {
            std::cout << "Flags (CF, ZF, OF, NF): " << carry << " " << resultIsZero << " " << overflow << " " << negative << std::endl;
            
            regFile.setFlag(RegisterFile::Flag::CF, carry);
            regFile.setFlag(RegisterFile::Flag::ZF, resultIsZero);
            regFile.setFlag(RegisterFile::Flag::OF, overflow);
            regFile.setFlag(RegisterFile::Flag::NF, negative);
        }

        return incrementPC;
    }
    
private:    
};

#undef instDebug

static void AddInstruction(Memory& mem, uint32_t opCode, uint32_t arg0, uint32_t arg1, bool arg1IsReg)
{
    static uint32_t nextAddr = 0u;
    
    std::cout << "5" << std::endl;
    std::cout << "arg1IsReg: " << arg1IsReg << std::endl;
    
    uint32_t inst = 0;
    inst |= (opCode << 27);
    std::cout << inst << std::endl;
    inst |= (arg0 << 22);
    std::cout << inst << std::endl;
    inst |= ((arg1IsReg ? 0x01ul : 0x00ul) << 21);
    std::cout << inst << std::endl;
    inst |= ((arg1 & 0xFFFFul) << 5);
    std::cout << inst << std::endl;
    
    mem.write(nextAddr, inst);
    
    std::cout << "(O: 0x" << std::hex << opCode << ", A0: 0x" << std::hex << arg0 << ", A1: 0x" << std::hex << arg1 << ") Writing to: 0x" << std::hex << nextAddr << " val 0x" << inst << std::endl;
    
/*
-------+----------------------------------------------------+
| Name | OpCode |  arg0  |  arg1  | Reserved for future use |
-------+----------------------------------------------------+
| Bits | 31..27 | 26..22 | 21..5  |         4..0            |
-------+----------------------------------------------------+
 */

    nextAddr += 4ul;
}

static void AddInstruction(Memory& mem, OpCode opCode, Reg arg0)
{
    std::cout << "1" << std::endl;
    AddInstruction(mem, static_cast<uint32_t>(opCode), static_cast<uint32_t>(arg0), static_cast<uint32_t>(0x00000000ul), true);
}

static void AddInstruction(Memory& mem, OpCode opCode, Reg arg0, Reg arg1)
{
    std::cout << "2" << std::endl;
    AddInstruction(mem, static_cast<uint32_t>(opCode), static_cast<uint32_t>(arg0), static_cast<uint32_t>(arg1), true);
}

static void AddInstruction(Memory& mem, OpCode opCode, uint16_t arg0)
{
    std::cout << "3" << std::endl;
    if ((opCode == OpCode::jmp) || (opCode == OpCode::jeq) || (opCode == OpCode::jne) || (opCode == OpCode::jgt) || (opCode == OpCode::jlt) || (opCode == OpCode::jge) || (opCode == OpCode::jle))
    {
        // When JMP instruction jumps to a constant address then it is encoded with the first argument being empty and the second argument being the constant value
        AddInstruction(mem, static_cast<uint32_t>(opCode), static_cast<uint32_t>(0x00000000ul), static_cast<uint32_t>(arg0), false);
    }
    else
    {
        std::cout << "Invalid instruction and argument combination" << std::endl;
        exit(1);
    }
}

static void AddInstruction(Memory& mem, OpCode opCode, Reg arg0, uint16_t arg1)
{
    std::cout << "4" << std::endl;
    AddInstruction(mem, static_cast<uint32_t>(opCode), static_cast<uint32_t>(arg0), static_cast<uint32_t>(arg1), false);
}

int main(int argc, char* argv[])
{
    static constexpr uint32_t ROMBaseAddr = 0x00000000ul;
    static constexpr uint32_t RAMBaseAddr = 0x000FFFF0ul;
    
    Memory ROM(0xFFul + 1ul, true);
    Memory RAM(0xFFFFul + 1ul, false);
    MemAddrDecodeRAMROM<ROMBaseAddr, RAMBaseAddr> memDec(ROM, RAM);
    

    
    RegisterFile regFile;
    
    std::cout << "-------------- Setup --------------" << std::endl;
    
//#define NORMAL_TEST
//#define NORMAL_TEST2
#define GPIO_TEST
    
#if defined(NORMAL_TEST)
    // Exception Vector:
    //     0x00000000 - Entry point addr
    //     0x00000004 - Invalid instruction
    //     0x00000008 - Invalid address
    AddInstruction(ROM, OpCode::jmp, 0x0000000Cul);      //0x0000 // Jump to after the exception vector
    AddInstruction(ROM, OpCode::jmp, 0x00000004ul);      //0x0004
    AddInstruction(ROM, OpCode::jmp, 0x00000008ul);      //0x0008

    AddInstruction(ROM, OpCode::ldu, Reg::R0, 0xAABBul); //0x000C
    AddInstruction(ROM, OpCode::ldl, Reg::R0, 0xCCDDul); //0x0010
    AddInstruction(ROM, OpCode::ldl, Reg::R1, 0x00DDul); //0x0014
    AddInstruction(ROM, OpCode::sub, Reg::R0, Reg::R1);  //0x0018
    AddInstruction(ROM, OpCode::ldu, Reg::R2, 0x000Ful); //0x001C
    AddInstruction(ROM, OpCode::ldl, Reg::R2, 0xFFF0ul); //0x0020
    AddInstruction(ROM, OpCode::str, Reg::R0, Reg::R2);  //0x0024
    AddInstruction(ROM, OpCode::ldr, Reg::R3, Reg::R2);  //0x0028
    AddInstruction(ROM, OpCode::mov, Reg::R4, Reg::R3);  //0x002C
    AddInstruction(ROM, OpCode::cmp, Reg::R4, Reg::R3);  //0x0030
    AddInstruction(ROM, OpCode::jeq, 0x0000003Cul);      //0x0034
    AddInstruction(ROM, OpCode::sub, Reg::R4, Reg::R2);  //0x0038
    AddInstruction(ROM, OpCode::cmp, Reg::R4, Reg::R3);  //0x003C
    AddInstruction(ROM, OpCode::jlt, 0x00000050ul);      //0x0040
    AddInstruction(ROM, OpCode::jmp, 0x00000038ul);      //0x0044
    AddInstruction(ROM, OpCode::add, Reg::R4, Reg::R2);  //0x0048
    AddInstruction(ROM, OpCode::add, Reg::R4, Reg::R2);  //0x004C
    AddInstruction(ROM, OpCode::cmp, Reg::R4, Reg::R3);  //0x0050
    AddInstruction(ROM, OpCode::jgt, 0x0000005Cul);      //0x0054
    AddInstruction(ROM, OpCode::jmp, 0x00000048ul);      //0x0058
    AddInstruction(ROM, OpCode::cmp, Reg::R4, Reg::R3);  //0x005C
    AddInstruction(ROM, OpCode::jne, 0x00000068ul);      //0x0060
    AddInstruction(ROM, OpCode::jmp, Reg::R5);           //0x0064
    AddInstruction(ROM, OpCode::ldl, Reg::R5, 0x0070u);  //0x0068
    AddInstruction(ROM, OpCode::cmp, Reg::R1, Reg::R0);  //0x006C
    AddInstruction(ROM, OpCode::jge, 0x00000078ul);      //0x0070
    AddInstruction(ROM, OpCode::jmp, 0x00000074ul);      //0x0074
    AddInstruction(ROM, OpCode::ldu, Reg::R0, 0x0000ul); //0x0078
    AddInstruction(ROM, OpCode::ldl, Reg::R0, 0x00DDul); //0x007C
    AddInstruction(ROM, OpCode::cmp, Reg::R0, Reg::R1);  //0x0080
    AddInstruction(ROM, OpCode::jle, 0x0000008Cul);      //0x0084
    AddInstruction(ROM, OpCode::dec, Reg::R0);           //0x0088
    AddInstruction(ROM, OpCode::cmp, Reg::R0, Reg::R1);  //0x008C
    AddInstruction(ROM, OpCode::jle, 0x00000098ul);      //0x0090
    AddInstruction(ROM, OpCode::jmp, 0x00000094ul);      //0x0094
    AddInstruction(ROM, OpCode::inc, Reg::R0);           //0x0098
    AddInstruction(ROM, OpCode::inc, Reg::R0);           //0x009C
    AddInstruction(ROM, OpCode::cmp, Reg::R0, Reg::R1);  //0x00A0
    AddInstruction(ROM, OpCode::jge, 0x000000ACul);      //0x00A4
    AddInstruction(ROM, OpCode::jmp, 0x000000A8ul);      //0x00A8
    AddInstruction(ROM, OpCode::ldu, Reg::SP, (RAMBaseAddr + 0xFFul) >> 16ul);    //0x00AC
    AddInstruction(ROM, OpCode::ldl, Reg::SP, (RAMBaseAddr + 0xFFul) & 0xFFFFul); //0x00B0
    AddInstruction(ROM, OpCode::push, Reg::R0);          //0x00B4
    AddInstruction(ROM, OpCode::add, Reg::R0, Reg::R1);  //0x00B8
    AddInstruction(ROM, OpCode::pop, Reg::R0);           //0x00BC
    AddInstruction(ROM, OpCode::dec, Reg::R0);           //0x00C0
    AddInstruction(ROM, OpCode::dec, Reg::R0);           //0x00C4
    AddInstruction(ROM, OpCode::jmp, 0x000000C8ul);      //0x00C8

#elif defined(NORMAL_TEST2)
    // Exception Vector:
    // Exception Vector:
    //     0x00000000 - Entry point addr
    //     0x00000004 - Invalid instruction
    //     0x00000008 - Invalid address
    AddInstruction(ROM, OpCode::jmp, 0x0000000Cul);      //0x0000 // Jump to after the exception vector
    AddInstruction(ROM, OpCode::jmp, 0x00000004ul);      //0x0004
    AddInstruction(ROM, OpCode::jmp, 0x00000008ul);      //0x0008

    AddInstruction(ROM, OpCode::inc, Reg::R0);           //0x000C
    AddInstruction(ROM, OpCode::ldu, Reg::R0, 0xAABBul); //0x0010
    AddInstruction(ROM, OpCode::ldl, Reg::R0, 0xCCDDul); //0x0014
    AddInstruction(ROM, OpCode::ldu, Reg::R2, 0x000Ful); //0x0018
    AddInstruction(ROM, OpCode::ldl, Reg::R2, 0xFFF0ul); //0x001C
    AddInstruction(ROM, OpCode::str, Reg::R0, Reg::R2);  //0x0020
    AddInstruction(ROM, OpCode::ldr, Reg::R3, Reg::R2);  //0x0024
    AddInstruction(ROM, OpCode::add, Reg::R0, Reg::R3);  //0x0028
    AddInstruction(ROM, OpCode::cmp, Reg::R0, Reg::R3);  //0x002C
    AddInstruction(ROM, OpCode::jgt, 0x00000038ul);      //0x0030
    AddInstruction(ROM, OpCode::jmp, 0x00000034ul);      //0x0034
    AddInstruction(ROM, OpCode::cmp, Reg::R3, Reg::R0);  //0x0038
    AddInstruction(ROM, OpCode::jlt, 0x00000048ul);      //0x003C
    AddInstruction(ROM, OpCode::cmp, Reg::R5, Reg::R6);  //0x0040
    AddInstruction(ROM, OpCode::jeq, 0x00000050ul);      //0x0044
    AddInstruction(ROM, OpCode::jne, 0x00000040ul);      //0x0048
    AddInstruction(ROM, OpCode::jmp, 0x0000004Cul);      //0x004C
    AddInstruction(ROM, OpCode::ldl, Reg::R4, 0x0001);   //0x0050
    AddInstruction(ROM, OpCode::shl, Reg::R0, Reg::R4);  //0x0054
    AddInstruction(ROM, OpCode::ldl, Reg::R5, 0x0002);   //0x0058
    AddInstruction(ROM, OpCode::shr, Reg::R0, Reg::R5);  //0x005C
    AddInstruction(ROM, OpCode::mov, Reg::R4, Reg::R5);  //0x0060
    AddInstruction(ROM, OpCode::jmp, 0x00000064ul);      //0x0064
    AddInstruction(ROM, OpCode::jmp, 0x00000068ul);      //0x0068

#elif defined(GPIO_TEST)    
    // Exception Vector:
    //     0x00000000 - Entry point addr
    //     0x00000004 - Invalid instruction
    //     0x00000008 - Invalid address
    AddInstruction(ROM, OpCode::jmp, 0x0000000Cul);      //0x0000 // Jump to after the exception vector
    AddInstruction(ROM, OpCode::jmp, 0x00000004ul);      //0x0004
    AddInstruction(ROM, OpCode::jmp, 0x00000008ul);      //0x0008

    // r0 = 0xFFFFFF00 (GPIO bank base address)
    AddInstruction(ROM, OpCode::ldu, Reg::R0, 0xFFFFul); //0x000C
    AddInstruction(ROM, OpCode::ldl, Reg::R0, 0xFF00ul); //0x0010

    // r1 = 0x00000000
    AddInstruction(ROM, OpCode::ldu, Reg::R1, 0x0000ul); //0x0014
    AddInstruction(ROM, OpCode::ldl, Reg::R1, 0x0000ul); //0x0018
    
    // r2 = 0xFFFFFFFE
    AddInstruction(ROM, OpCode::ldu, Reg::R2, 0xFFFFul); //0x001C
    AddInstruction(ROM, OpCode::ldl, Reg::R2, 0xFFFEul); //0x0020
    
    // r3 = [r0]
    AddInstruction(ROM, OpCode::ldr, Reg::R3, Reg::R0);  //0x0024
    
    // r1 = ~r1
    AddInstruction(ROM, OpCode::Not, Reg::R1);           //0x0028
    
    // r1 = r1 & r2
    AddInstruction(ROM, OpCode::And, Reg::R1, Reg::R2);  //0x002C
    
    // r3 = r3 | r1
    AddInstruction(ROM, OpCode::orr, Reg::R3, Reg::R1);  //0x0030
    
    // [r0] = r3
    AddInstruction(ROM, OpCode::str, Reg::R1, Reg::R0);  //0x0034
    
    
    
    
    
    AddInstruction(ROM, OpCode::ldu, Reg::R4, 0x0000ul);  //0x0038
    AddInstruction(ROM, OpCode::ldl, Reg::R4, 0x0000ul);  //0x003C
     
    AddInstruction(ROM, OpCode::ldu, Reg::R5, 0x0000ul);  //0x0040
    AddInstruction(ROM, OpCode::ldl, Reg::R5, 0x0040ul);  //0x0044
     
    AddInstruction(ROM, OpCode::jmp, 0x00000050ul);       //0x0048
    AddInstruction(ROM, OpCode::inc, Reg::R4);            //0x004C
    
    AddInstruction(ROM, OpCode::cmp, Reg::R4, Reg::R5);   //0x0050
    AddInstruction(ROM, OpCode::jlt, 0x0000004Cul);       //0x0054
    
    AddInstruction(ROM, OpCode::jmp, 0x00000024ul);       //0x0058
     
//     AddInstruction(ROM, OpCode::shr, Reg::R0, Reg::R5);  //0x005C
//     AddInstruction(ROM, OpCode::mov, Reg::R4, Reg::R5);  //0x0060
//     AddInstruction(ROM, OpCode::jmp, 0x00000064ul);      //0x0064
//     AddInstruction(ROM, OpCode::jmp, 0x00000068ul);      //0x0068
#else
    #error "No code to execute"
#endif

    if (argc > 1)
    {
        ROM.saveAsVerilogArray(std::string(argv[1]) + "/ROMData.v");
    }

    std::cout << "-----------------------------------" << std::endl;
    std::cout << std::endl;
    

    uint32_t clockCycleCount = 0;
    regFile.writePC(0);
    
    while(clockCycleCount < 52)
    {
        std::cout << "Clock Cycle: " << std::dec << clockCycleCount << std::endl;
        
        if (regFile.memStoreQueued())
        {
            std::cout << "Executing memory store" << std::endl;
            
            regFile.markMemStoreAsDone();

            if (!memDec.write(regFile.read(regFile.queuedStoreReg1()), regFile.read(regFile.queuedStoreReg0())))
            {
                ExceptionLogic::raiseInvalidAddr(regFile);
            }
        }
        else if (regFile.memLoadQueued())
        {
            std::cout << "Executing memory load" << std::endl;
            
            regFile.markMemLoadAsDone();
            
            uint32_t readData = 0xDEADBEEF;
            if (!memDec.read(regFile.read(regFile.queuedLoadReg1()), readData))
            {                
                ExceptionLogic::raiseInvalidAddr(regFile);
            }
            else
            {
                regFile.write(regFile.queuedLoadReg0(), readData);
            }
        }
        else
        {
            uint32_t fetchedInstruction = 0xFFFFFFFFu;
            const bool fetched = InstFetch::fetch(regFile, memDec, fetchedInstruction);
            if (!fetched)
            {
                ExceptionLogic::raiseInvalidAddr(regFile);
            }
            else
            {
                std::cout << "Fetched instruction: 0x" << std::hex << fetchedInstruction << std::endl;
                
                uint16_t opCode = 0xFFFFu;
                uint32_t arg0 = 0xFFFFu;
                uint32_t arg1 = 0xFFFFu; 
                bool arg1IsReg = false;
                const bool decodeSuccess = InstDec::decode(fetchedInstruction, opCode, arg0, arg1, arg1IsReg);
                if (!decodeSuccess)
                {
                    ExceptionLogic::raiseInvalidInstruction(regFile);
                }
                else
                {
                    const bool incrementPC = ExecuteInstruction::execute(opCode, arg0, arg1, arg1IsReg, regFile);
                    if (incrementPC)
                    {
                        regFile.writePC(regFile.readPC() + 4ul);
                    }
                }
            }
        }
        
        clockCycleCount++;
        
        std::cout << std::endl;
    }
    
    return 0;
}
