`default_nettype none   // Don't allow undeclared nets

module TCPU_Top
#(parameter IO_CHANNEL_COUNT = 1)
( 
    input wire i_clock,
    input wire i_asyncRSTn,
    output wire [(IO_CHANNEL_COUNT - 1):0] o_output
);
/* verilator lint_off UNUSED */
    wire w_rstN;
/* verilator lint_on UNUSED */

    AsyncResetSynchronizer AsyncRstSync
    (
        .i_clock(i_clock),
        .i_asyncRSTn(i_asyncRSTn),
        .o_RSTn(w_rstN)
    );

    TCPU tcpu
    (
        .i_clock(i_clock),
        .i_RSTn(w_rstN),
        .o_output(o_output)
    );

endmodule 
 
