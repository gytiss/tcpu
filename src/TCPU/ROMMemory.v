`default_nettype none   // Don't allow undeclared nets

module ROMMemory
#(parameter MEMORY_SIZE_IN_WORDS = 32'd256)
(
    input wire i_clock,
    input wire i_RSTn,
    input wire i_enabled,
    
    input wire [31:0] i_address,
    inout wire [31:0] io_data
);
    `include "ROMData.v"

    always @ (posedge i_clock or negedge i_RSTn)
    begin
        if (!i_RSTn) 
            begin

            end
    end

    assign io_data = (((i_enabled == 1'b1) && (i_address < (MEMORY_SIZE_IN_WORDS * 32'd4))) ? a_ROM[i_address / 32'd4] : 32'hZZZZ_ZZZZ);

endmodule
