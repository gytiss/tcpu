`default_nettype none   // Don't allow undeclared nets

module RAMMemory
#(parameter MEMORY_SIZE_IN_WORDS = 32'd256)
(
    input wire i_clock,
    input wire i_RSTn,
    input wire i_enabled,
    
    input wire [31:0] i_address,
    input wire i_writing,
    inout wire [31:0] io_data
);
    reg [31:0] a_RAM [MEMORY_SIZE_IN_WORDS - 1:0];

    always @ (posedge i_clock or negedge i_RSTn)
    begin
        if (!i_RSTn) 
            begin

            end
        else
            if (i_enabled)
                begin
                    if (i_writing && (i_address < (MEMORY_SIZE_IN_WORDS * 32'd4)))
                        a_RAM[i_address / 32'd4] <= io_data; 
                end
    end

    assign io_data = (((i_writing == 1'b0) && (i_enabled == 1'b1) && (i_address < (MEMORY_SIZE_IN_WORDS *  32'd4))) ? a_RAM[i_address / 32'd4] : 32'hZZZZ_ZZZZ);

endmodule
