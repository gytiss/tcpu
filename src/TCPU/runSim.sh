#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

rm -f ${SCRIPT_DIR}TCPUSim

g++ -Wall --std=c++14 -g ${SCRIPT_DIR}/TCPUSim.cpp -o ${SCRIPT_DIR}/TCPUSim

if [ $? -eq 0 ]
then
    if [ "x${1}" == "xvalgrind" ]; then
        valgrind ${SCRIPT_DIR}/TCPUSim ${SCRIPT_DIR}
    else
        ${SCRIPT_DIR}/TCPUSim ${SCRIPT_DIR}
    fi
fi

cd ${SCRIPT_DIR}
