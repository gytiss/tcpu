`default_nettype none   // Don't allow undeclared nets

module ExecutionUnit
#(
    parameter INSTRUCTION_OPCODE_JMP  = 0,    
    parameter INSTRUCTION_OPCODE_JEQ  = 0,    
    parameter INSTRUCTION_OPCODE_JNE  = 0,    
    parameter INSTRUCTION_OPCODE_JGT  = 0,    
    parameter INSTRUCTION_OPCODE_JLT  = 0,    
    parameter INSTRUCTION_OPCODE_JGE  = 0,    
    parameter INSTRUCTION_OPCODE_JLE  = 0,    
    parameter INSTRUCTION_OPCODE_ADD  = 0,    
    parameter INSTRUCTION_OPCODE_SUB  = 0,    
    parameter INSTRUCTION_OPCODE_MUL  = 0,    
    parameter INSTRUCTION_OPCODE_INC  = 0,    
    parameter INSTRUCTION_OPCODE_DEC  = 0,    
    parameter INSTRUCTION_OPCODE_XOR  = 0,    
    parameter INSTRUCTION_OPCODE_ORR  = 0,    
    parameter INSTRUCTION_OPCODE_AND  = 0,    
    parameter INSTRUCTION_OPCODE_NOT  = 0,    
    parameter INSTRUCTION_OPCODE_SHL  = 0,    
    parameter INSTRUCTION_OPCODE_SHR  = 0,    
    parameter INSTRUCTION_OPCODE_CMP  = 0,    
    parameter INSTRUCTION_OPCODE_MOV  = 0,    
    parameter INSTRUCTION_OPCODE_LDR  = 0,    
    parameter INSTRUCTION_OPCODE_LDU  = 0,  
    parameter INSTRUCTION_OPCODE_LDL  = 0,    
    parameter INSTRUCTION_OPCODE_STR  = 0,  
    parameter INSTRUCTION_OPCODE_PUSH = 0, 
    parameter INSTRUCTION_OPCODE_POP  = 0,

    parameter REGISTER_INDEX_R0  = 0,
    parameter REGISTER_INDEX_R1  = 0,
    parameter REGISTER_INDEX_R2  = 0,
    parameter REGISTER_INDEX_R3  = 0,
    parameter REGISTER_INDEX_R4  = 0,
    parameter REGISTER_INDEX_R5  = 0,
    parameter REGISTER_INDEX_R6  = 0,
    parameter REGISTER_INDEX_R7  = 0,
    parameter REGISTER_INDEX_R8  = 0,
    parameter REGISTER_INDEX_R9  = 0,
    parameter REGISTER_INDEX_R10 = 0,
    parameter REGISTER_INDEX_R11 = 0,
    parameter REGISTER_INDEX_R12 = 0,
    parameter REGISTER_INDEX_R13 = 0,
    parameter REGISTER_INDEX_R14 = 0,
    parameter REGISTER_INDEX_LR  = 0,
    parameter REGISTER_INDEX_SP  = 0,

    parameter REGISTER_ADDRESS_R0  = 0,
    parameter REGISTER_ADDRESS_R1  = 0,
    parameter REGISTER_ADDRESS_R2  = 0,
    parameter REGISTER_ADDRESS_R3  = 0,
    parameter REGISTER_ADDRESS_R4  = 0,
    parameter REGISTER_ADDRESS_R5  = 0,
    parameter REGISTER_ADDRESS_R6  = 0,
    parameter REGISTER_ADDRESS_R7  = 0,
    parameter REGISTER_ADDRESS_R8  = 0,
    parameter REGISTER_ADDRESS_R9  = 0,
    parameter REGISTER_ADDRESS_R10 = 0,
    parameter REGISTER_ADDRESS_R11 = 0,
    parameter REGISTER_ADDRESS_R12 = 0,
    parameter REGISTER_ADDRESS_R13 = 0,
    parameter REGISTER_ADDRESS_R14 = 0,
    parameter REGISTER_ADDRESS_LR  = 0,
    parameter REGISTER_ADDRESS_SP  = 0
)
(
    input wire i_clock,
    input wire i_RSTn,
    
    input wire [4:0]  i_instructionOpCode, 
    input wire [4:0]  i_instructionArg0, 
    input wire [15:0] i_instructionArg1, 
    input wire        i_instructionArg1IsAReg,
    input wire        i_instructionIsValid,
    
    input wire i_absoluteMemoryAddressIsInvalid,
    input wire i_absoluteMemoryAddressIsReadOnly,
    
    output reg [26:0] o_instructionBeingExecuted,
    
    output wire o_writeDataToMemory,
    output wire [31:0] o_absoluteMemoryAddress,
    inout wire [31:0] io_memoryDataBus
);

    localparam CPU_STAGE_FETCH               = 5'b00001;
    localparam CPU_STAGE_DECODE              = 5'b00010;
    localparam CPU_STAGE_EXECUTGE            = 5'b00100; 
    localparam CPU_STAGE_MEMORY_ACCESS       = 5'b01000;
    localparam CPU_STAGE_REGISTER_WRITE_BACK = 5'b10000;

    // Processor Flags State
    reg r_cpuFlagsSettingRequested;
/* verilator lint_off UNUSED */
    reg r_cpuFlagCF; // ALU op resulted in a carry
/* verilator lint_on UNUSED */
    reg r_cpuFlagZF; // ALU operation result is zero
    reg r_cpuFlagOF; // ALU op resulted in an overflow (either negative or positive)
    reg r_cpuFlagNF; // ALU op operation resulted in a negative number
    
    // Execution Unit State
    reg [31:0] r_nextInstructionAddress;  
    reg [4:0]  r_currentCPUState;
    reg        r_jumpInstructionExecuted;
    reg [31:0] r_jumpAddress;
    
    // Register Write Access State
    reg [31:0] r_regWriteAddr;
    reg [31:0] r_regWriteData;
    reg r_registerWriteBackRequested;
    wire w_writeDataToTheRegisterFile;
    
    // Stack Pointer Register Write Access State
    reg [31:0] r_stackPointerRegWriteData;
    reg r_stackPointerRegisterWriteBackRequested;
    wire w_writeDataToTheStackPointerRegister;
    
    // Register Read Access State
    wire [31:0] w_reg0ReadAddr;
    wire [31:0] w_reg1ReadAddr;
    reg [31:0] r_reg0ReadData;
    reg [31:0] r_reg1ReadData;
    wire [31:0] w_reg0Data;
    wire [31:0] w_reg1Data;
    
    // Instruction Register Address State
    wire [31:0] w_instructionRegAddr0;
    wire [31:0] w_instructionRegAddr1;

    // ALU State
    reg [31:0] r_aluOperand0;
/* verilator lint_off UNUSED */
    reg [31:0] r_aluOperand1;
/* verilator lint_on UNUSED */
    reg [6:0]  r_aluOperation;
    wire [31:0] w_aluResult;
    wire w_aluFlagCarry;
    wire w_aluFlagBorrow;
    wire w_aluFlagOverflow;
    wire w_aluFlagResultIsZero;
    wire w_aluFlagResultIsNegative;

    // Barrel Shifter State
    reg [31:0] r_barrselShifterOperand;
/* verilator lint_off UNUSED */
    reg [5:0]  r_barrselShifterShiftCount;
/* verilator lint_on UNUSED */
    reg        r_barrselShifterShiftRightRequested;
    reg [31:0] r_barrselShifterResult;


    // Memory I/O State
    reg r_memoryWriteRequested;
    reg r_memoryReadRequested;
    reg [31:0] r_memoryIOAddr;
    reg [31:0] r_dataToWriteToMemory;
    
    // Memory I/O
    assign o_writeDataToMemory = (|(r_currentCPUState & CPU_STAGE_MEMORY_ACCESS) && r_memoryWriteRequested);
    assign o_absoluteMemoryAddress = (((|(r_currentCPUState & CPU_STAGE_MEMORY_ACCESS)) == 1'b1) || ((|(r_currentCPUState & CPU_STAGE_REGISTER_WRITE_BACK)) == 1'b1) ? r_memoryIOAddr : r_nextInstructionAddress);
    assign io_memoryDataBus = (o_writeDataToMemory ? r_dataToWriteToMemory : 32'hZZZZ_ZZZZ);
    
    // Register Write Access
    assign w_writeDataToTheRegisterFile = (|(r_currentCPUState & CPU_STAGE_REGISTER_WRITE_BACK) && r_registerWriteBackRequested);
    
    // Stack Pointer Register Write Access State
    assign w_writeDataToTheStackPointerRegister = (|(r_currentCPUState & CPU_STAGE_REGISTER_WRITE_BACK) && r_stackPointerRegisterWriteBackRequested);
    
    // Register Read Access
    localparam INVALID_REGISTER_ADDRESS = 32'hFFFFFFFF;
    assign w_reg0ReadAddr = w_instructionRegAddr0;
    assign w_reg1ReadAddr = (i_instructionArg1IsAReg ? w_instructionRegAddr1 : INVALID_REGISTER_ADDRESS);
    assign w_reg0Data = r_reg0ReadData;
    assign w_reg1Data = (i_instructionArg1IsAReg ? r_reg1ReadData : { 16'd0, i_instructionArg1 });

    // Exception Vector:
    //     0x00000000 - Entry point addr
    //     0x00000004 - Invalid instruction
    //     0x00000008 - Invalid address
    localparam INVALID_ISTRUCTION_EXCEPTION_HANDLER_ADDR = 32'h00000004;
    localparam INVALID_ADDRESS_EXCEPTION_HANDLER_ADDR = 32'h00000008;
    wire w_invalidAddressExceptionRaised = (i_absoluteMemoryAddressIsInvalid | (i_absoluteMemoryAddressIsReadOnly & r_memoryWriteRequested));

    RegisterFile
    #(
        .REGISTER_INDEX_R0(REGISTER_INDEX_R0),
        .REGISTER_INDEX_R1(REGISTER_INDEX_R1),
        .REGISTER_INDEX_R2(REGISTER_INDEX_R2),
        .REGISTER_INDEX_R3(REGISTER_INDEX_R3),
        .REGISTER_INDEX_R4(REGISTER_INDEX_R4),
        .REGISTER_INDEX_R5(REGISTER_INDEX_R5),
        .REGISTER_INDEX_R6(REGISTER_INDEX_R6),
        .REGISTER_INDEX_R7(REGISTER_INDEX_R7),
        .REGISTER_INDEX_R8(REGISTER_INDEX_R8),
        .REGISTER_INDEX_R9(REGISTER_INDEX_R9),
        .REGISTER_INDEX_R10(REGISTER_INDEX_R10),
        .REGISTER_INDEX_R11(REGISTER_INDEX_R11),
        .REGISTER_INDEX_R12(REGISTER_INDEX_R12),
        .REGISTER_INDEX_R13(REGISTER_INDEX_R13),
        .REGISTER_INDEX_R14(REGISTER_INDEX_R14),
        .REGISTER_INDEX_LR(REGISTER_INDEX_LR),
        .REGISTER_INDEX_SP(REGISTER_INDEX_SP),
        .REGISTER_ADDRESS_R0(REGISTER_ADDRESS_R0),
        .REGISTER_ADDRESS_R1(REGISTER_ADDRESS_R1),
        .REGISTER_ADDRESS_R2(REGISTER_ADDRESS_R2),
        .REGISTER_ADDRESS_R3(REGISTER_ADDRESS_R3),
        .REGISTER_ADDRESS_R4(REGISTER_ADDRESS_R4),
        .REGISTER_ADDRESS_R5(REGISTER_ADDRESS_R5),
        .REGISTER_ADDRESS_R6(REGISTER_ADDRESS_R6),
        .REGISTER_ADDRESS_R7(REGISTER_ADDRESS_R7),
        .REGISTER_ADDRESS_R8(REGISTER_ADDRESS_R8),
        .REGISTER_ADDRESS_R9(REGISTER_ADDRESS_R9),
        .REGISTER_ADDRESS_R10(REGISTER_ADDRESS_R10),
        .REGISTER_ADDRESS_R11(REGISTER_ADDRESS_R11),
        .REGISTER_ADDRESS_R12(REGISTER_ADDRESS_R12),
        .REGISTER_ADDRESS_R13(REGISTER_ADDRESS_R13),
        .REGISTER_ADDRESS_R14(REGISTER_ADDRESS_R14),
        .REGISTER_ADDRESS_LR(REGISTER_ADDRESS_LR),
        .REGISTER_ADDRESS_SP(REGISTER_ADDRESS_SP)
    )
    registerFile
    (
        .i_clock(i_clock),
        .i_RSTn(i_RSTn),

        .i_writingEnabled(w_writeDataToTheRegisterFile),
        .i_regWriteAddr(r_regWriteAddr),
        .i_regWriteData(r_regWriteData),
        
        .i_writingStackPointerRegEnabled(w_writeDataToTheStackPointerRegister),
        .i_stackPointerRegWriteData(r_stackPointerRegWriteData),

        .i_regReadAddr0(w_reg0ReadAddr),
        .i_regReadAddr1(w_reg1ReadAddr),
        .o_regReadData0(r_reg0ReadData),
        .o_regReadData1(r_reg1ReadData)
    );

    RegisterAddressDecoder
    #(
        .REGISTER_INDEX_R0(REGISTER_INDEX_R0),
        .REGISTER_INDEX_R1(REGISTER_INDEX_R1),
        .REGISTER_INDEX_R2(REGISTER_INDEX_R2),
        .REGISTER_INDEX_R3(REGISTER_INDEX_R3),
        .REGISTER_INDEX_R4(REGISTER_INDEX_R4),
        .REGISTER_INDEX_R5(REGISTER_INDEX_R5),
        .REGISTER_INDEX_R6(REGISTER_INDEX_R6),
        .REGISTER_INDEX_R7(REGISTER_INDEX_R7),
        .REGISTER_INDEX_R8(REGISTER_INDEX_R8),
        .REGISTER_INDEX_R9(REGISTER_INDEX_R9),
        .REGISTER_INDEX_R10(REGISTER_INDEX_R10),
        .REGISTER_INDEX_R11(REGISTER_INDEX_R11),
        .REGISTER_INDEX_R12(REGISTER_INDEX_R12),
        .REGISTER_INDEX_R13(REGISTER_INDEX_R13),
        .REGISTER_INDEX_R14(REGISTER_INDEX_R14),
        .REGISTER_INDEX_LR(REGISTER_INDEX_LR),
        .REGISTER_INDEX_SP(REGISTER_INDEX_SP),
        .REGISTER_ADDRESS_R0(REGISTER_ADDRESS_R0),
        .REGISTER_ADDRESS_R1(REGISTER_ADDRESS_R1),
        .REGISTER_ADDRESS_R2(REGISTER_ADDRESS_R2),
        .REGISTER_ADDRESS_R3(REGISTER_ADDRESS_R3),
        .REGISTER_ADDRESS_R4(REGISTER_ADDRESS_R4),
        .REGISTER_ADDRESS_R5(REGISTER_ADDRESS_R5),
        .REGISTER_ADDRESS_R6(REGISTER_ADDRESS_R6),
        .REGISTER_ADDRESS_R7(REGISTER_ADDRESS_R7),
        .REGISTER_ADDRESS_R8(REGISTER_ADDRESS_R8),
        .REGISTER_ADDRESS_R9(REGISTER_ADDRESS_R9),
        .REGISTER_ADDRESS_R10(REGISTER_ADDRESS_R10),
        .REGISTER_ADDRESS_R11(REGISTER_ADDRESS_R11),
        .REGISTER_ADDRESS_R12(REGISTER_ADDRESS_R12),
        .REGISTER_ADDRESS_R13(REGISTER_ADDRESS_R13),
        .REGISTER_ADDRESS_R14(REGISTER_ADDRESS_R14),
        .REGISTER_ADDRESS_LR(REGISTER_ADDRESS_LR),
        .REGISTER_ADDRESS_SP(REGISTER_ADDRESS_SP)
    )
    registerAddressDecoder0
    (
        .i_regIndex(i_instructionArg0),
        .o_regAddress(w_instructionRegAddr0)
    );

    RegisterAddressDecoder
    #(
        .REGISTER_INDEX_R0(REGISTER_INDEX_R0),
        .REGISTER_INDEX_R1(REGISTER_INDEX_R1),
        .REGISTER_INDEX_R2(REGISTER_INDEX_R2),
        .REGISTER_INDEX_R3(REGISTER_INDEX_R3),
        .REGISTER_INDEX_R4(REGISTER_INDEX_R4),
        .REGISTER_INDEX_R5(REGISTER_INDEX_R5),
        .REGISTER_INDEX_R6(REGISTER_INDEX_R6),
        .REGISTER_INDEX_R7(REGISTER_INDEX_R7),
        .REGISTER_INDEX_R8(REGISTER_INDEX_R8),
        .REGISTER_INDEX_R9(REGISTER_INDEX_R9),
        .REGISTER_INDEX_R10(REGISTER_INDEX_R10),
        .REGISTER_INDEX_R11(REGISTER_INDEX_R11),
        .REGISTER_INDEX_R12(REGISTER_INDEX_R12),
        .REGISTER_INDEX_R13(REGISTER_INDEX_R13),
        .REGISTER_INDEX_R14(REGISTER_INDEX_R14),
        .REGISTER_INDEX_LR(REGISTER_INDEX_LR),
        .REGISTER_INDEX_SP(REGISTER_INDEX_SP),
        .REGISTER_ADDRESS_R0(REGISTER_ADDRESS_R0),
        .REGISTER_ADDRESS_R1(REGISTER_ADDRESS_R1),
        .REGISTER_ADDRESS_R2(REGISTER_ADDRESS_R2),
        .REGISTER_ADDRESS_R3(REGISTER_ADDRESS_R3),
        .REGISTER_ADDRESS_R4(REGISTER_ADDRESS_R4),
        .REGISTER_ADDRESS_R5(REGISTER_ADDRESS_R5),
        .REGISTER_ADDRESS_R6(REGISTER_ADDRESS_R6),
        .REGISTER_ADDRESS_R7(REGISTER_ADDRESS_R7),
        .REGISTER_ADDRESS_R8(REGISTER_ADDRESS_R8),
        .REGISTER_ADDRESS_R9(REGISTER_ADDRESS_R9),
        .REGISTER_ADDRESS_R10(REGISTER_ADDRESS_R10),
        .REGISTER_ADDRESS_R11(REGISTER_ADDRESS_R11),
        .REGISTER_ADDRESS_R12(REGISTER_ADDRESS_R12),
        .REGISTER_ADDRESS_R13(REGISTER_ADDRESS_R13),
        .REGISTER_ADDRESS_R14(REGISTER_ADDRESS_R14),
        .REGISTER_ADDRESS_LR(REGISTER_ADDRESS_LR),
        .REGISTER_ADDRESS_SP(REGISTER_ADDRESS_SP)
    )
    registerAddressDecoder1
    (
        .i_regIndex(i_instructionArg1[4:0]),
        .o_regAddress(w_instructionRegAddr1)
    );

    localparam ALU_OP_ADD = 7'b0000000;
    localparam ALU_OP_INC = 7'b0000001;
    localparam ALU_OP_SUB = 7'b0000010;
    localparam ALU_OP_DEC = 7'b0000011;
    localparam ALU_OP_MUL = 7'b0000100;
    localparam ALU_OP_XOR = 7'b0001000;
    localparam ALU_OP_OR  = 7'b0010000;
    localparam ALU_OP_AND = 7'b0100000;
    localparam ALU_OP_NOT = 7'b1000000;
    ALU #(.OPERATION_WIDTH(32'd32)) alu
    (
        .i_operand0(r_aluOperand0),
        .i_operand1(r_aluOperand1),
        .i_operation(r_aluOperation),
        .o_result(w_aluResult),
        .o_carry(w_aluFlagCarry),
        .o_borrow(w_aluFlagBorrow),
        .o_overflow(w_aluFlagOverflow),
        .o_resultIsZero(w_aluFlagResultIsZero),
        .o_resultIsNegative(w_aluFlagResultIsNegative)
    );
    
    BarrelShifter #(.OPERATION_WIDTH(32'd32)) shifter 
    (
        .i_operand0(r_barrselShifterOperand),
        .i_shiftCount(r_barrselShifterShiftCount),
        .i_shiftRight(r_barrselShifterShiftRightRequested),
        .o_result(r_barrselShifterResult)
    );

    always @ (posedge i_clock or negedge i_RSTn)
    begin
        if (!i_RSTn) 
            begin
                r_nextInstructionAddress <= 32'd0;
                r_currentCPUState <= CPU_STAGE_FETCH;
            end
        else
            begin
                if (r_currentCPUState == CPU_STAGE_FETCH)
                    begin
                        o_instructionBeingExecuted <= io_memoryDataBus[31:5];
                        r_currentCPUState <= CPU_STAGE_DECODE;
                    end
                else if (r_currentCPUState == CPU_STAGE_DECODE)
                    begin
                        r_currentCPUState <= CPU_STAGE_EXECUTGE;
                    end
                else if (r_currentCPUState == CPU_STAGE_EXECUTGE)
                    begin
                        if (i_instructionIsValid == 1'b0)
                            begin
                                r_nextInstructionAddress <= INVALID_ISTRUCTION_EXCEPTION_HANDLER_ADDR;
                                r_currentCPUState <= CPU_STAGE_MEMORY_ACCESS;
                            end
                        else
                        begin
                            if (r_cpuFlagsSettingRequested == 1'b1)
                            begin
                                r_cpuFlagCF <= (w_aluFlagCarry | ((|(r_aluOperation & ALU_OP_SUB)) & ~w_aluFlagBorrow));
                                r_cpuFlagZF <= w_aluFlagResultIsZero;
                                r_cpuFlagOF <= w_aluFlagOverflow;
                                r_cpuFlagNF <= w_aluFlagResultIsNegative;
                            end

                            if ((r_memoryWriteRequested == 1'b1) || (r_memoryReadRequested == 1'b1) || (r_jumpInstructionExecuted == 1'b1))
                                r_currentCPUState <= CPU_STAGE_MEMORY_ACCESS;
                            else
                                r_currentCPUState <= CPU_STAGE_REGISTER_WRITE_BACK;
                        end
                    end
                else if (r_currentCPUState == CPU_STAGE_MEMORY_ACCESS)
                    begin
                        if (i_instructionIsValid == 1'b0)
                            r_currentCPUState <= CPU_STAGE_FETCH;
                        else if (w_invalidAddressExceptionRaised == 1'b1)
                            begin
                                r_nextInstructionAddress <= INVALID_ADDRESS_EXCEPTION_HANDLER_ADDR;
                                r_currentCPUState <= CPU_STAGE_FETCH;
                            end
                        else if (r_jumpInstructionExecuted == 1'b1)
                            begin
                                r_nextInstructionAddress <= r_jumpAddress;
                                r_currentCPUState <= CPU_STAGE_FETCH;
                            end
                        else
                            r_currentCPUState <= CPU_STAGE_REGISTER_WRITE_BACK;
                    end
                else if (r_currentCPUState == CPU_STAGE_REGISTER_WRITE_BACK)
                    begin
                        r_nextInstructionAddress <= (r_nextInstructionAddress + 32'd4);

                        r_currentCPUState <= CPU_STAGE_FETCH;
                    end

            end
    end

    task WriteDataToTheStackPointerRegister(input reg [31:0] i_regWriteData);
        begin
            r_stackPointerRegWriteData = i_regWriteData;
            r_stackPointerRegisterWriteBackRequested = 1'b1;
        end
    endtask

    task WriteDataToARegister(input reg [31:0] i_regWriteAddr, input reg [31:0] i_regWriteData);
        begin
            r_regWriteData = i_regWriteData;
            r_regWriteAddr = i_regWriteAddr;
            r_registerWriteBackRequested = 1'b1;
        end
    endtask


    task ReadDataFromMemory(input reg [31:0] i_memoryIOAddress);
        begin
            r_memoryIOAddr = i_memoryIOAddress;
            r_memoryReadRequested = 1'b1;
        end
    endtask
    
    task WriteDataToMemory(input reg [31:0] i_memoryIOAddress, input reg [31:0] i_dataToWriteToMemory);
        begin
            r_dataToWriteToMemory = i_dataToWriteToMemory;
            r_memoryIOAddr = i_memoryIOAddress;
            r_memoryWriteRequested = 1'b1;
        end
    endtask

    
    task PerformShiftOperation(input reg [31:0] i_operand, input reg [5:0] i_shiftCount, input reg i_shiftRightRequested);
        begin
            r_barrselShifterOperand = i_operand;
            r_barrselShifterShiftCount = i_shiftCount;
            r_barrselShifterShiftRightRequested = i_shiftRightRequested;
        end
    endtask
    
    task PerformALUOperation(input reg [6:0] i_operation, input reg [31:0] i_operand0, input reg [31:0] i_operand1);
        begin
            r_aluOperation = i_operation;
            r_aluOperand0 = i_operand0;
            r_aluOperand1 = i_operand1;
        end
    endtask

    
    task PerformJump(input reg executeJump);
        begin
            if  (executeJump)
                begin
                    if (!i_instructionArg1IsAReg)
                        r_jumpAddress = w_reg1Data;
                    else
                        r_jumpAddress = w_reg0Data;

                    r_jumpInstructionExecuted = 1'b1; 
                end
        end
    endtask

    reg [31:0] r_stackPointerValueAfterAPush;

    always_comb
        begin
            r_stackPointerValueAfterAPush = 32'd0;
            r_stackPointerRegWriteData = 32'd0;
            r_stackPointerRegisterWriteBackRequested = 1'b0;

            r_jumpAddress = 32'd0;
            r_jumpInstructionExecuted = 1'b0;
            r_regWriteData = 32'd0;
            r_regWriteAddr = 32'd0;
            r_registerWriteBackRequested = 1'b0;
            r_memoryWriteRequested = 1'b0;
            r_memoryReadRequested = 1'b0;
            r_memoryIOAddr = 32'd0;
            r_dataToWriteToMemory = 32'd0;
            r_cpuFlagsSettingRequested = 1'b0;

            r_barrselShifterOperand = 32'd0;
            r_barrselShifterShiftCount = 6'd0;
            r_barrselShifterShiftRightRequested = 1'b0;
            
            r_aluOperation = 7'd0;
            r_aluOperand0 = 32'd0;
            r_aluOperand1 = 32'd0;

//
//          Signed Comparison
//            LT = r_cpuFlagNF ^ r_cpuFlagOF
//            LE = r_cpuFlagZF | (r_cpuFlagNF ^ r_cpuFlagOF)
//            EQ = r_cpuFlagZF
//            NE = ~r_cpuFlagZF
//            GE = ~(r_cpuFlagNF ^ r_cpuFlagOF)
//            GT = ~(r_cpuFlagZF | (r_cpuFlagNF ^ r_cpuFlagOF))
//          
//          Unsigned Comparison
//            LTU = r_cpuFlagCF
//            LEU = r_cpuFlagCF | r_cpuFlagZF
//            GEU = ~r_cpuFlagCF
//            GTU = ~(r_cpuFlagCF | r_cpuFlagZF)
//
            if  (i_instructionIsValid == 1'b1)
                begin
                    case (i_instructionOpCode)
                        INSTRUCTION_OPCODE_JMP  : begin PerformJump(1'b1); end
                        INSTRUCTION_OPCODE_JEQ  : begin PerformJump(r_cpuFlagZF); end
                        INSTRUCTION_OPCODE_JNE  : begin PerformJump(~r_cpuFlagZF); end
                        INSTRUCTION_OPCODE_JGT  : begin PerformJump(~(r_cpuFlagZF | (r_cpuFlagNF ^ r_cpuFlagOF))); end
                        INSTRUCTION_OPCODE_JLT  : begin PerformJump(r_cpuFlagNF ^ r_cpuFlagOF); end
                        INSTRUCTION_OPCODE_JGE  : begin PerformJump(~(r_cpuFlagNF ^ r_cpuFlagOF)); end
                        INSTRUCTION_OPCODE_JLE  : begin PerformJump(r_cpuFlagZF | (r_cpuFlagNF ^ r_cpuFlagOF)); end
                        INSTRUCTION_OPCODE_ADD  : begin PerformALUOperation(ALU_OP_ADD, w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_SUB  : begin PerformALUOperation(ALU_OP_SUB, w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_MUL  : begin PerformALUOperation(ALU_OP_MUL, w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_INC  : begin PerformALUOperation(ALU_OP_INC, w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_DEC  : begin PerformALUOperation(ALU_OP_DEC, w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_XOR  : begin PerformALUOperation(ALU_OP_XOR, w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_ORR  : begin PerformALUOperation(ALU_OP_OR,  w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_AND  : begin PerformALUOperation(ALU_OP_AND, w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_NOT  : begin PerformALUOperation(ALU_OP_NOT, w_reg0Data, w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, w_aluResult); end
                        INSTRUCTION_OPCODE_SHL  : begin PerformShiftOperation(w_reg0Data, w_reg1Data[5:0], 1'b0); WriteDataToARegister(w_reg0ReadAddr, r_barrselShifterResult); end
                        INSTRUCTION_OPCODE_SHR  : begin PerformShiftOperation(w_reg0Data, w_reg1Data[5:0], 1'b1); WriteDataToARegister(w_reg0ReadAddr, r_barrselShifterResult); end
                        INSTRUCTION_OPCODE_CMP  : begin PerformALUOperation(ALU_OP_SUB, w_reg0Data, w_reg1Data); r_cpuFlagsSettingRequested = 1'b1; end
                        INSTRUCTION_OPCODE_MOV  : begin WriteDataToARegister(w_reg0ReadAddr, w_reg1Data); end
                        INSTRUCTION_OPCODE_LDR  : begin ReadDataFromMemory(w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, io_memoryDataBus); end
                        INSTRUCTION_OPCODE_LDU  : begin WriteDataToARegister(w_reg0ReadAddr, {w_reg1Data[15:0], w_reg0Data[15:0]}); end
                        INSTRUCTION_OPCODE_LDL  : begin WriteDataToARegister(w_reg0ReadAddr, {w_reg0Data[31:16], w_reg1Data[15:0]}); end
                        INSTRUCTION_OPCODE_STR  : begin WriteDataToMemory(w_reg1Data, w_reg0Data); end
                        INSTRUCTION_OPCODE_PUSH : begin r_stackPointerValueAfterAPush = w_aluResult; PerformALUOperation(ALU_OP_SUB, w_reg1Data, 32'd4); WriteDataToMemory(r_stackPointerValueAfterAPush, w_reg0Data); WriteDataToTheStackPointerRegister(r_stackPointerValueAfterAPush); end 
                        INSTRUCTION_OPCODE_POP  : begin ReadDataFromMemory(w_reg1Data); WriteDataToARegister(w_reg0ReadAddr, io_memoryDataBus); PerformALUOperation(ALU_OP_ADD, w_reg1Data, 32'd4); WriteDataToTheStackPointerRegister(w_aluResult); end

                        default : ;
                    endcase
                end           
        end

endmodule
