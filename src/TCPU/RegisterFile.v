`default_nettype none   // Don't allow undeclared nets

module RegisterFile
#(
    parameter REGISTER_INDEX_R0   = 0,
    parameter REGISTER_INDEX_R1   = 0,
    parameter REGISTER_INDEX_R2   = 0,
    parameter REGISTER_INDEX_R3   = 0,
    parameter REGISTER_INDEX_R4   = 0,
    parameter REGISTER_INDEX_R5   = 0,
    parameter REGISTER_INDEX_R6   = 0,
    parameter REGISTER_INDEX_R7   = 0,
    parameter REGISTER_INDEX_R8   = 0,
    parameter REGISTER_INDEX_R9   = 0,
    parameter REGISTER_INDEX_R10  = 0,
    parameter REGISTER_INDEX_R11  = 0,
    parameter REGISTER_INDEX_R12  = 0,
    parameter REGISTER_INDEX_R13  = 0,
    parameter REGISTER_INDEX_R14  = 0,
    parameter REGISTER_INDEX_LR   = 0,
    parameter REGISTER_INDEX_SP   = 0,

    parameter REGISTER_ADDRESS_R0 = 0,
    parameter REGISTER_ADDRESS_R1 = 0,
    parameter REGISTER_ADDRESS_R2 = 0,
    parameter REGISTER_ADDRESS_R3 = 0,
    parameter REGISTER_ADDRESS_R4 = 0,
    parameter REGISTER_ADDRESS_R5 = 0,
    parameter REGISTER_ADDRESS_R6 = 0,
    parameter REGISTER_ADDRESS_R7 = 0,
    parameter REGISTER_ADDRESS_R8 = 0,
    parameter REGISTER_ADDRESS_R9 = 0,
    parameter REGISTER_ADDRESS_R10 = 0,
    parameter REGISTER_ADDRESS_R11 = 0,
    parameter REGISTER_ADDRESS_R12 = 0,
    parameter REGISTER_ADDRESS_R13 = 0,
    parameter REGISTER_ADDRESS_R14 = 0,
    parameter REGISTER_ADDRESS_LR  = 0,
    parameter REGISTER_ADDRESS_SP  = 0
)
(
    input wire i_clock,
    input wire i_RSTn,    

    input wire        i_writingEnabled,
    input wire [31:0] i_regWriteAddr,
    input wire [31:0] i_regWriteData,

    input wire        i_writingStackPointerRegEnabled,
    input wire [31:0] i_stackPointerRegWriteData,

    input wire [31:0] i_regReadAddr0,
    input wire [31:0] i_regReadAddr1,
    output reg [31:0] o_regReadData0,
    output reg [31:0] o_regReadData1
);

    reg [31:0] r_regs [16:0];

    always @ (posedge i_clock or negedge i_RSTn)
    begin
        if (!i_RSTn) 
            begin
                r_regs[REGISTER_INDEX_R0]  <= 32'd0;
                r_regs[REGISTER_INDEX_R1]  <= 32'd0;
                r_regs[REGISTER_INDEX_R2]  <= 32'd0;
                r_regs[REGISTER_INDEX_R3]  <= 32'd0;
                r_regs[REGISTER_INDEX_R4]  <= 32'd0;
                r_regs[REGISTER_INDEX_R5]  <= 32'd0;
                r_regs[REGISTER_INDEX_R6]  <= 32'd0;
                r_regs[REGISTER_INDEX_R7]  <= 32'd0;
                r_regs[REGISTER_INDEX_R8]  <= 32'd0;
                r_regs[REGISTER_INDEX_R9]  <= 32'd0;
                r_regs[REGISTER_INDEX_R10] <= 32'd0;
                r_regs[REGISTER_INDEX_R11] <= 32'd0;
                r_regs[REGISTER_INDEX_R12] <= 32'd0;
                r_regs[REGISTER_INDEX_R13] <= 32'd0;
                r_regs[REGISTER_INDEX_R14] <= 32'd0;
                r_regs[REGISTER_INDEX_LR]  <= 32'd0;
                r_regs[REGISTER_INDEX_SP]  <= 32'd0;
            end
        else
            begin
                if (i_writingEnabled == 1'b1)
                    begin
                        case(i_regWriteAddr)
                            REGISTER_ADDRESS_R0  : r_regs[REGISTER_INDEX_R0]  <= i_regWriteData;
                            REGISTER_ADDRESS_R1  : r_regs[REGISTER_INDEX_R1]  <= i_regWriteData;
                            REGISTER_ADDRESS_R2  : r_regs[REGISTER_INDEX_R2]  <= i_regWriteData;
                            REGISTER_ADDRESS_R3  : r_regs[REGISTER_INDEX_R3]  <= i_regWriteData;
                            REGISTER_ADDRESS_R4  : r_regs[REGISTER_INDEX_R4]  <= i_regWriteData;
                            REGISTER_ADDRESS_R5  : r_regs[REGISTER_INDEX_R5]  <= i_regWriteData;
                            REGISTER_ADDRESS_R6  : r_regs[REGISTER_INDEX_R6]  <= i_regWriteData;
                            REGISTER_ADDRESS_R7  : r_regs[REGISTER_INDEX_R7]  <= i_regWriteData;
                            REGISTER_ADDRESS_R8  : r_regs[REGISTER_INDEX_R8]  <= i_regWriteData;
                            REGISTER_ADDRESS_R9  : r_regs[REGISTER_INDEX_R9]  <= i_regWriteData;
                            REGISTER_ADDRESS_R10 : r_regs[REGISTER_INDEX_R10] <= i_regWriteData;
                            REGISTER_ADDRESS_R11 : r_regs[REGISTER_INDEX_R11] <= i_regWriteData;
                            REGISTER_ADDRESS_R12 : r_regs[REGISTER_INDEX_R12] <= i_regWriteData;
                            REGISTER_ADDRESS_R13 : r_regs[REGISTER_INDEX_R13] <= i_regWriteData;
                            REGISTER_ADDRESS_R14 : r_regs[REGISTER_INDEX_R14] <= i_regWriteData;
                            REGISTER_ADDRESS_LR  : r_regs[REGISTER_INDEX_LR]  <= i_regWriteData;
                            REGISTER_ADDRESS_SP  : if (i_writingStackPointerRegEnabled == 1'b0) r_regs[REGISTER_INDEX_SP] <= i_regWriteData;
                        endcase
                    end

                if (i_writingStackPointerRegEnabled == 1'b1)
                    begin
                        r_regs[REGISTER_INDEX_SP] <= i_stackPointerRegWriteData;
                    end
            end
    end

    always_comb
    begin
        case(i_regReadAddr0)
            REGISTER_ADDRESS_R0  : o_regReadData0 = r_regs[REGISTER_INDEX_R0];
            REGISTER_ADDRESS_R1  : o_regReadData0 = r_regs[REGISTER_INDEX_R1];
            REGISTER_ADDRESS_R2  : o_regReadData0 = r_regs[REGISTER_INDEX_R2];
            REGISTER_ADDRESS_R3  : o_regReadData0 = r_regs[REGISTER_INDEX_R3];
            REGISTER_ADDRESS_R4  : o_regReadData0 = r_regs[REGISTER_INDEX_R4];
            REGISTER_ADDRESS_R5  : o_regReadData0 = r_regs[REGISTER_INDEX_R5];
            REGISTER_ADDRESS_R6  : o_regReadData0 = r_regs[REGISTER_INDEX_R6];
            REGISTER_ADDRESS_R7  : o_regReadData0 = r_regs[REGISTER_INDEX_R7];
            REGISTER_ADDRESS_R8  : o_regReadData0 = r_regs[REGISTER_INDEX_R8];
            REGISTER_ADDRESS_R9  : o_regReadData0 = r_regs[REGISTER_INDEX_R9];
            REGISTER_ADDRESS_R10 : o_regReadData0 = r_regs[REGISTER_INDEX_R10];
            REGISTER_ADDRESS_R11 : o_regReadData0 = r_regs[REGISTER_INDEX_R11];
            REGISTER_ADDRESS_R12 : o_regReadData0 = r_regs[REGISTER_INDEX_R12];
            REGISTER_ADDRESS_R13 : o_regReadData0 = r_regs[REGISTER_INDEX_R13];
            REGISTER_ADDRESS_R14 : o_regReadData0 = r_regs[REGISTER_INDEX_R14];
            REGISTER_ADDRESS_LR  : o_regReadData0 = r_regs[REGISTER_INDEX_LR];
            REGISTER_ADDRESS_SP  : o_regReadData0 = r_regs[REGISTER_INDEX_SP];
            default: o_regReadData0 = 32'hDEADBEEF;
        endcase

        case(i_regReadAddr1)
            REGISTER_ADDRESS_R0  : o_regReadData1 = r_regs[REGISTER_INDEX_R0];
            REGISTER_ADDRESS_R1  : o_regReadData1 = r_regs[REGISTER_INDEX_R1];
            REGISTER_ADDRESS_R2  : o_regReadData1 = r_regs[REGISTER_INDEX_R2];
            REGISTER_ADDRESS_R3  : o_regReadData1 = r_regs[REGISTER_INDEX_R3];
            REGISTER_ADDRESS_R4  : o_regReadData1 = r_regs[REGISTER_INDEX_R4];
            REGISTER_ADDRESS_R5  : o_regReadData1 = r_regs[REGISTER_INDEX_R5];
            REGISTER_ADDRESS_R6  : o_regReadData1 = r_regs[REGISTER_INDEX_R6];
            REGISTER_ADDRESS_R7  : o_regReadData1 = r_regs[REGISTER_INDEX_R7];
            REGISTER_ADDRESS_R8  : o_regReadData1 = r_regs[REGISTER_INDEX_R8];
            REGISTER_ADDRESS_R9  : o_regReadData1 = r_regs[REGISTER_INDEX_R9];
            REGISTER_ADDRESS_R10 : o_regReadData1 = r_regs[REGISTER_INDEX_R10];
            REGISTER_ADDRESS_R11 : o_regReadData1 = r_regs[REGISTER_INDEX_R11];
            REGISTER_ADDRESS_R12 : o_regReadData1 = r_regs[REGISTER_INDEX_R12];
            REGISTER_ADDRESS_R13 : o_regReadData1 = r_regs[REGISTER_INDEX_R13];
            REGISTER_ADDRESS_R14 : o_regReadData1 = r_regs[REGISTER_INDEX_R14];
            REGISTER_ADDRESS_LR  : o_regReadData1 = r_regs[REGISTER_INDEX_LR];
            REGISTER_ADDRESS_SP  : o_regReadData1 = r_regs[REGISTER_INDEX_SP];
            default: o_regReadData1 = 32'hDEADBEEF;
        endcase
    end

endmodule
