#!/usr/bin/env bash 

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

FPGA_TOOLS_DIR=~/Programs/LatticeFPGATools

ICE_STORM_BINS_DIR=$FPGA_TOOLS_DIR/icestorm/bin
ARACHNE_PNR_BINS_DIR=$FPGA_TOOLS_DIR/arachne-pnr/bin
NEXTPNR_BINS_DIR=$FPGA_TOOLS_DIR/nextpnr/bin
YOSYS_BINS_DIR=$FPGA_TOOLS_DIR/yosys/bin

export PATH=$ICE_STORM_BINS_DIR/bin:$NEXTPNR_BINS_DIR/bin:$YOSYS_BINS_DIR/bin:$PATH

cd $SCRIPT_DIR

GENERATED_FPGA_BINARIES_DIR=$SCRIPT_DIR/bins

mkdir -p $GENERATED_FPGA_BINARIES_DIR


VERILOG_MODULE_NAME=TCPU_Top_FPGA
#VERILOG_MODULE_FILE_PATH=$SCRIPT_DIR/src/$VERILOG_MODULE_NAME.v
#OUTPUT_FILE_PATH=$SCRIPT_DIR/src/$VERILOG_MODULE_NAME.out

# -defer
#         only read the abstract syntax tree and defer actual compilation
#         to a later 'hierarchy' command. Useful in cases where the default
#         parameters of modules yield invalid or not synthesizable code.
# 
# -noautowire
#         make the default of `default_nettype be "none" instead of "wire".

yosys -f "verilog -sv -defer -noautowire" -p "synth_ice40 -top $VERILOG_MODULE_NAME -json $GENERATED_FPGA_BINARIES_DIR/$VERILOG_MODULE_NAME.json" /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/TCPU_Top_FPGA.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/TCPU.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/ROMMemory.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/RAMMemory.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/AddrDecoder.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/RegisterFile.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/InstructionDecoder.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/RegisterAddressDecoder.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/ExecutionUnit.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/../ALU/ALU.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/../ALU/Adder.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/../ALU/Multiplier.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/../ALU/Subtracter.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/../ALU/MUX2.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/../ALU/MUX7.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/../BarrelShifter/BarrelShifter.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/GPIOModule.v /home/bendras/projects/VerilogPlayground/UTs/src/TCPU/../CommonVerilog/AsyncResetSynchronizer.v

if [ $? -eq 0 ]
then
    nextpnr-ice40 --hx8k --package ct256  --json $GENERATED_FPGA_BINARIES_DIR/$VERILOG_MODULE_NAME.json --pcf $SCRIPT_DIR/ice40hx8k-bb.pcf --asc $GENERATED_FPGA_BINARIES_DIR/$VERILOG_MODULE_NAME.asc
    
    if [ $? -eq 0 ]
    then
        icepack $GENERATED_FPGA_BINARIES_DIR/$VERILOG_MODULE_NAME.asc $GENERATED_FPGA_BINARIES_DIR/$VERILOG_MODULE_NAME.bin
        
        if [ $? -eq 0 ]
        then
            echo "To load"
            iceprog $GENERATED_FPGA_BINARIES_DIR/$VERILOG_MODULE_NAME.bin
        fi
    fi
fi
