
##########################################
#                                        #
#  Macros shared between all test cases  #
#                                        #
##########################################
macro(ADD_VERILOG_SOURCE verilogSource)
    set (VERILOG_SOURCES ${VERILOG_SOURCES} "${CMAKE_CURRENT_SOURCE_DIR}/${verilogSource}")
endmacro(ADD_VERILOG_SOURCE)

macro(ADD_VERILOG_DEPENDENCY verilogDependency)
    set (VERILOG_DEPENDENCIES ${VERILOG_DEPENDENCIES} "${CMAKE_CURRENT_SOURCE_DIR}/${verilogDependency}")
endmacro(ADD_VERILOG_SOURCE)

macro(ADD_TEST_BENCH_SOURCE testBenchSource)
    set (TEST_BENCH_SOURCES ${TEST_BENCH_SOURCES} "${CMAKE_CURRENT_SOURCE_DIR}/${testBenchSource}")
endmacro(ADD_TEST_BENCH_SOURCE) 
