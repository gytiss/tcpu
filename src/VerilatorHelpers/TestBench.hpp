#ifndef TEST_BENCH_HPP_INCLUDED 
#define TEST_BENCH_HPP_INCLUDED

#ifdef UT_VERILATOR_TRACING_ENABLED
    #if (UT_VERILATOR_TRACING_ENABLED == UTS_VERILATOR_TRACING_FILE_FORMAT_VCD)
        #include <verilated_vcd_c.h>
    #elif (UT_VERILATOR_TRACING_ENABLED == UTS_VERILATOR_TRACING_FILE_FORMAT_FST)
        #include <verilated_fst_c.h>
    #else
        #error "Unsupported tracing format"
    #endif
#else
    #include <verilated.h>
#endif

#include <cstdint>
#include <cassert>
#include <vector>
#include <memory>
#include <tuple>

#include "CommonDefinitions.hpp"
#include "TestBenchClock.hpp"

template<class MODULE> class TestBench 
{
public:
    typedef void (*ClockTickCallback_t)(TestBench<MODULE>* thisPtr);

    /**
     * @param mandatoryClockClockRateUnit - Clock rate unit of the "i_clock" in verilog module
     * @param mandatoryClockClockRate     - Clock rate unit of the "i_clock" in verilog module
     * @param mandatoryClockClockTickCallback  - Callback function which will be called every time "i_clock" clock ticks
     *
     * Note: Having at least one clock is mandatory, hence it is added in the constructor
     */
    TestBench(FreqUnit mandatoryClockClockRateUnit,
              uint64_t mandatoryClockClockRate,
              ClockTickCallback_t mandatoryClockClockTickCallback)
    : m_simultationTimeInPs(0ull),
      m_clocks(),
      m_core(new MODULE())

#ifdef UT_VERILATOR_TRACING_ENABLED
      ,
      m_trace(nullptr)
#endif
    {
#ifdef UT_VERILATOR_TRACING_ENABLED        
        // According to the Verilator spec, you *must* call
        // traceEverOn before calling any of the tracing functions
        // within Verilator.
        Verilated::traceEverOn(true);
#endif

        constexpr uint32_t MandatoryClockID = 0u;
        _addClock(mandatoryClockClockRateUnit, mandatoryClockClockRate, mandatoryClockClockTickCallback, &(m_core->i_clock), MandatoryClockID);        
    }

    virtual ~TestBench() = default;
    TestBench(const TestBench&) = delete;   
    TestBench(TestBench&&) = delete;

#ifdef UT_VERILATOR_TRACING_ENABLED
    // Open/create a trace file
    void opentrace(const char* traceFileName) 
    {
        if (!m_trace) 
        {
            #if (UT_VERILATOR_TRACING_ENABLED == UTS_VERILATOR_TRACING_FILE_FORMAT_VCD)
                m_trace.reset(new VerilatedVcdC());

                // Note: Call order is important here. Calling either "set_time_unit()" or "set_time_resolution()" after "open()" is called would have no effect.
                m_trace->spTrace()->set_time_unit(std::to_string(1) + _timeUnitToTimeUnitStr(TimeUnit::ps));
                m_trace->spTrace()->set_time_resolution(std::to_string(1) + _timeUnitToTimeUnitStr(TimeUnit::ps));

                m_core->trace(m_trace.get(), 99);
                m_trace->open(traceFileName);

            #elif (UT_VERILATOR_TRACING_ENABLED == UTS_VERILATOR_TRACING_FILE_FORMAT_FST)
                m_trace.reset(new VerilatedFstC());

                m_core->trace(m_trace.get(), 99);
            
                // Note: Call order is important here. Calling either "set_time_unit()" or "set_time_resolution()" before "open()" is called would have no effect.
                m_trace->open(traceFileName);
                m_trace->spTrace()->set_time_unit(std::to_string(1) + _timeUnitToTimeUnitStr(TimeUnit::ps));
                m_trace->spTrace()->set_time_resolution(std::to_string(1) + _timeUnitToTimeUnitStr(TimeUnit::ps));
            #else
                #error "Unsupported tracing format"
            #endif
        }
    }

    // Close a trace file
    void closetrace() 
    {
        if (m_trace) 
        {
            m_trace->close();
            m_trace.reset();
        }
    }
    
    inline uint64_t currentSimulationTimeInTicks() const
    {
        return m_simultationTimeInPs;
    }
#endif
    
#ifdef UT_VERILOG_TEST_BENCH_IS_CONTINUOUS
    virtual void setupBeforeContinuousTest() = 0;

    void continuousTest(volatile bool& terminationRequested)
    {
        setupBeforeContinuousTest();

        while (!done() && !terminationRequested) 
        {
            tick();
        }
    }
#else
    virtual void test() = 0;
#endif

#if !defined(UT_VERILOG_TEST_BENCH_IS_CONTINUOUS) || defined(ENABLE_RESET_SIGNAL_HANDLING_IN_TEST_BENCH)
#if defined(ENABLE_RESET_SIGNAL_HANDLING_IN_TEST_BENCH) && defined(UT_VERILOG_TEST_BENCH_IS_CONTINUOUS)
private:
#endif
    void waitForNTicks(uint64_t ticksToWaitFor, uint32_t clockID)
    {
        const TestBenchClock* clock = _getClockWithID(clockID);
        
        // Clock with the given ID does not exist 
        assert(clock != nullptr);

        if (clock != nullptr)
        {
            const uint64_t startTickCount = clock->clockTicksSoFar();

            while (!done() && ((clock->clockTicksSoFar() - startTickCount) < ticksToWaitFor)) 
            {
                tick();
            }
        }
    }
    
    void waitForNTimeUnits(TimeUnit timeUnit, uint64_t timeUnitsToWaitFor)
    {
        const uint64_t startTimeInPs = m_simultationTimeInPs;
        const uint64_t timeUnitsToWaitForInPs = TestBenchClock::timeUnitToPicoseconds(timeUnit, timeUnitsToWaitFor);

        while (!done() && ((m_simultationTimeInPs - startTimeInPs) < timeUnitsToWaitForInPs)) 
        {
            this->tick();
        }  
    }
    
    template<typename T>
    void waitTillVarEqual(T* varToPoll, const vluint64_t valueToWaitFor)
    {
        while (!done() && (*varToPoll != valueToWaitFor)) 
        {
            this->tick();
        }  
    }
    
    uint64_t clockTickCount(uint32_t clockID)
    {
        uint64_t retVal = 0;
        
        const TestBenchClock* clock = _getClockWithID(clockID);
        
        // Clock with the given ID does not exist 
        assert(clock != nullptr);

        if (clock != nullptr)
        {
            retVal = clock->clockTicksSoFar();
        }
        
        return retVal;
    }
#if defined(ENABLE_RESET_SIGNAL_HANDLING_IN_TEST_BENCH) && defined(UT_VERILOG_TEST_BENCH_IS_CONTINUOUS)
public:
#endif
#endif

#ifdef ENABLE_RESET_SIGNAL_HANDLING_IN_TEST_BENCH
    virtual void reset(TimeUnit timeUnit, uint64_t timeUnitsToWaitForAfterResetIsAsserted) 
    {
        m_core->i_asyncRSTn = 0;
        waitForNTimeUnits(timeUnit, timeUnitsToWaitForAfterResetIsAsserted);
        m_core->i_asyncRSTn = 1;
    }
#endif

    void final()
    {
        m_core->final();
    }

    void tick() 
    {            
        const uint64_t minimumTimeStepInPs = _minimumTimeStepInPsToAdvanceTheClocks();
        
        // Time step cannot be zero
        assert(minimumTimeStepInPs > 1);

        
        // Pre-evaluate, to give verilator a chance
        // to settle any combinatorial logic that
        // that may have changed since the last clock
        // evaluation
        m_core->eval();
#ifdef UT_VERILATOR_TRACING_ENABLED
        if (m_trace)
        {
            m_trace->dump(m_simultationTimeInPs);
        }
#endif

        // Advance time of all of the clocks
        for (auto& clockDataTuple: m_clocks)
        {
            vluint8_t* clockInput = std::get<0>(clockDataTuple);
            TestBenchClock& clock = std::get<1>(clockDataTuple);
            
            *clockInput = clock.advance(minimumTimeStepInPs);
        }

        // Evaluate simulated logic after advancing the clocks
        m_core->eval();
        m_simultationTimeInPs += minimumTimeStepInPs;

#ifdef UT_VERILATOR_TRACING_ENABLED
        if (m_trace) 
        {
            m_trace->dump(m_simultationTimeInPs);
            m_trace->flush();
        }
#endif
        for (auto& clockDataTuple: m_clocks)
        {
            TestBenchClock& clock = std::get<1>(clockDataTuple);
            if (clock.fallingEdge())
            {
                ClockTickCallback_t clockTickCallback = std::get<2>(clockDataTuple);
                if (clockTickCallback != nullptr)
                {
                    clockTickCallback(this);
                }
            }
        }
    }

    virtual bool done() 
    { 
        return Verilated::gotFinish();
    }
    
protected:
    inline MODULE& verilogModule()
    {
        return *m_core;
    }
    
    /**
     * Add a clock.
     * 
     * Note: The "i_clock" clock is automatically added during construction of the test bench. Adding it second time is neither required nor allowed.
     * 
     * @param clockRateUnit - Clock rate unit of the clock to be added
     * @param clockRate     - Clock rate of the clock to be added
     * @param tickCallback  - Callback function which will be called every time this clock ticks
     * @param clockInput    - Pointer to a clock input which will be toggled by the simulation. Basically "&(m_core-><Name_of_the_clock_input_signal_in_verilog_module>)".
     * @param clockID       - A random ID which will be used to identify the clock when calling clock related functions such as "waitForNTicks()". 
     *                        Note: The following are not allowed: 1) Adding a clock with ID zero (zero is the default clock which is always present). 2) Adding a clock with the same ID more than once.
     */
    void addClock(FreqUnit clockRateUnit,
                  uint64_t clockRate,
                  ClockTickCallback_t tickCallback,
                  vluint8_t* clockInput,
                  const uint32_t clockID)
    {
        // The "i_clock" clock is automatically added during construction of the test bench. Adding it second time is not allowed.
        assert(&(m_core->i_clock) != clockInput);
        
        _addClock(clockRateUnit, clockRate, tickCallback, clockInput, clockID);
    }

private:
    void _addClock(FreqUnit clockRateUnit,
                   uint64_t clockRate,
                   ClockTickCallback_t tickCallback,
                   vluint8_t* clockInput,
                   const uint32_t clockID)
    {
        // Clock must always be associated with a clock input
        assert(clockInput != nullptr);

        // Adding more than one clock with the same clock ID is not allowed
        assert(_getClockWithID(clockID) == nullptr);

        m_clocks.emplace_back(std::forward_as_tuple(clockInput, TestBenchClock{clockRateUnit, clockRate}, tickCallback, clockID));
    }
    
    uint64_t _minimumTimeStepInPsToAdvanceTheClocks() const
    {
        uint64_t minimumTimeStepInPs = std::get<1>(m_clocks[0]).timeToEdgeInPs();

        for (size_t i = 1; i < m_clocks.size(); i++)
        {
            const uint64_t timeToEdgeInPs = std::get<1>(m_clocks[i]).timeToEdgeInPs();
            if (timeToEdgeInPs < minimumTimeStepInPs)
            {
                minimumTimeStepInPs = timeToEdgeInPs;
            }
        }
        
        return minimumTimeStepInPs;
    }

    const TestBenchClock* _getClockWithID(uint32_t clockID)
    {
        TestBenchClock* retVal = nullptr;

        for (size_t i = 0; ((i < m_clocks.size()) && (retVal == nullptr)); i++)
        {
            const uint32_t currentClockID = std::get<3>(m_clocks[i]);
            if (currentClockID == clockID)
            {
                retVal = &(std::get<1>(m_clocks[i]));
            }
        }
        
        return retVal;
    }
    
    
#ifdef UT_VERILATOR_TRACING_ENABLED       
    const std::string _timeUnitToTimeUnitStr(TimeUnit timeUnit) const
    {

        std::string timeUnitStr = "s"; // Default to seconds
        switch(timeUnit)
        {
            case TimeUnit::s: timeUnitStr = "s"; break;
            case TimeUnit::ms: timeUnitStr = "ms"; break;
            case TimeUnit::us: timeUnitStr = "us"; break;
            case TimeUnit::ns: timeUnitStr = "ns"; break;
            case TimeUnit::ps: timeUnitStr = "ps"; break;
            //case TimeUnit::fs: timeUnitStr = "fs"; break;
            //case TimeUnit::as: timeUnitStr = "as"; break;
        }

        return timeUnitStr;
    }
#endif
    /**
     * -----------------------------------------
     * | Precision | Time Till Overflow        |
     * -----------------------------------------
     * |    s      |  213503982334 * 1000 days |
     * |    ms     |  213503982334  days       |
     * |    us     |  213503982,334 days       |
     * |    ns     |  213503,982334 days       |
     * |    ps     |  213,503982334 days       |
     * -----------------------------------------
     */
    uint64_t m_simultationTimeInPs; // Time in Picoseconds    
    std::vector<std::tuple<vluint8_t*, TestBenchClock, ClockTickCallback_t, uint32_t>> m_clocks;
    std::unique_ptr<MODULE> m_core;

#ifdef UT_VERILATOR_TRACING_ENABLED
    #if (UT_VERILATOR_TRACING_ENABLED == UTS_VERILATOR_TRACING_FILE_FORMAT_VCD)
        std::unique_ptr<VerilatedVcdC> m_trace;
    #elif (UT_VERILATOR_TRACING_ENABLED == UTS_VERILATOR_TRACING_FILE_FORMAT_FST)
        std::unique_ptr<VerilatedFstC> m_trace;
    #else
        #error "Unsupported tracing format"
    #endif
#endif
};

#endif // TEST_BENCH_HPP_INCLUDED
