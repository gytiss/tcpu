#ifndef TEST_BENCH_CLOCK_HPP_INCLUDED
#define TEST_BENCH_CLOCK_HPP_INCLUDED

#include <cstdint>
#include <cassert>
#include <verilated.h>

enum class TimeUnit
{
    s,  // Seconds
    ms, // Milliseconds
    us, // Microseconds
    ns, // Nanoseconds
    ps, // Picosecond
};

enum class FreqUnit
{
    Hz,  // Hertz
    kHz, // Kilohertz
    MHz, // Megahertz
    GHz, // Gigahertz 
    THz, // Terahertz
};

class TestBenchClock    
{
public: 
    static uint64_t frequencyUnitToHertz(FreqUnit freqUnit, uint64_t valueInGivenFreqUnit)
    {
        uint64_t retVal = 0ull;
        
        switch (freqUnit)
        {
            case FreqUnit::Hz: retVal = valueInGivenFreqUnit; break;
            case FreqUnit::kHz: retVal = (valueInGivenFreqUnit * 1000ull); break;
            case FreqUnit::MHz: retVal = (valueInGivenFreqUnit * 1000000ull); break;
            case FreqUnit::GHz: retVal = (valueInGivenFreqUnit * 1000000000ull); break;
            case FreqUnit::THz:  retVal = (valueInGivenFreqUnit * 1000000000000ull); break;
        }

        return retVal;
    }
    
    static uint64_t frequencyToPeriodInPs(FreqUnit freqUnit, uint64_t valueInGivenFreqUnit)
    {
        const uint64_t OneSecondInPicoseconds = 1000000000000ull;
        const uint64_t freqInHz = frequencyUnitToHertz(freqUnit, valueInGivenFreqUnit);

        return (OneSecondInPicoseconds / freqInHz);
    }

    static uint64_t timeUnitToPicoseconds(TimeUnit timeUnit, uint64_t valueInGivenTimeUnit)
    {
        uint64_t retVal = 0ull;
        
        switch (timeUnit)
        {
            case TimeUnit::s:  retVal = (valueInGivenTimeUnit * 1000000000000ull); break;
            case TimeUnit::ms: retVal = (valueInGivenTimeUnit * 1000000000ull); break;
            case TimeUnit::us: retVal = (valueInGivenTimeUnit * 1000000ull); break;
            case TimeUnit::ns: retVal = (valueInGivenTimeUnit * 1000ull); break;
            case TimeUnit::ps: retVal = valueInGivenTimeUnit; break;
        }

        return retVal;
    }

    TestBenchClock(FreqUnit clockRateUnit, uint64_t clockRate)
    : m_clockRateHalfPeriodInPs(frequencyToPeriodInPs(clockRateUnit, clockRate) / 2ull),
      m_currentClockRunningTimeInPs(m_clockRateHalfPeriodInPs),
      m_lastPositiveEdgeTimeInPs(0),
      m_ClockTickCount(0)
    {
        assert(m_clockRateHalfPeriodInPs > 0);
    }

    ~TestBenchClock() = default;
    TestBenchClock(const TestBenchClock&) = delete;   
    TestBenchClock(TestBenchClock&&) = default;
        
    uint64_t clockTicksSoFar() const
    { 
        return m_ClockTickCount;
    }

    uint64_t timeToEdgeInPs() const
    {
        uint64_t timeToEdgeInPs = 0;
        
        if (m_lastPositiveEdgeTimeInPs > m_currentClockRunningTimeInPs) 
        {
            // Should never happen
            assert(0);
        } 
        else if (m_lastPositiveEdgeTimeInPs + m_clockRateHalfPeriodInPs > m_currentClockRunningTimeInPs)
        {
            // Next edge is a negative edge
            timeToEdgeInPs = ((m_lastPositiveEdgeTimeInPs + m_clockRateHalfPeriodInPs) - m_currentClockRunningTimeInPs);
        }
        else if ((m_lastPositiveEdgeTimeInPs + (m_clockRateHalfPeriodInPs * 2ull)) > m_currentClockRunningTimeInPs)
        {
            // Next edge is a positive edge
            timeToEdgeInPs = ((m_lastPositiveEdgeTimeInPs + (m_clockRateHalfPeriodInPs * 2ull)) - m_currentClockRunningTimeInPs);
        }
        else 
        {
            // Should never happen
            assert(0);
        }

        return timeToEdgeInPs;
    }

    vluint8_t advance(uint64_t timeStepInPs) 
    {
        // Positive half of the clock's period
        vluint8_t clockEdge = 1;
        
        // Time step higher than the half period would mean clock skipping
        assert(timeStepInPs <= m_clockRateHalfPeriodInPs);

        m_currentClockRunningTimeInPs += timeStepInPs;

        if (m_currentClockRunningTimeInPs >= (m_lastPositiveEdgeTimeInPs + (m_clockRateHalfPeriodInPs * 2ull))) 
        {
            // Advance to the next positive edge
            m_lastPositiveEdgeTimeInPs += (m_clockRateHalfPeriodInPs * 2ull);
            m_ClockTickCount++;
            clockEdge = 1;
        } 
        else if (m_currentClockRunningTimeInPs >= (m_lastPositiveEdgeTimeInPs + m_clockRateHalfPeriodInPs)) 
        {
            // Negative half of the clock's period
            clockEdge = 0;
        }
        
        return clockEdge;
    }

    bool risingEdge()
    {
        if (m_currentClockRunningTimeInPs == m_lastPositiveEdgeTimeInPs)
        {
            return true;
        }
        
        return false;
    }

    bool fallingEdge()
    {
        if (m_currentClockRunningTimeInPs == m_lastPositiveEdgeTimeInPs + m_clockRateHalfPeriodInPs)
        {
            return true;
        }
        
        return false;
    }
    
private:
    uint64_t m_clockRateHalfPeriodInPs; 
    uint64_t m_currentClockRunningTimeInPs;
    uint64_t m_lastPositiveEdgeTimeInPs;
    uint64_t m_ClockTickCount;
};
#endif // TEST_BENCH_CLOCK_HPP_INCLUDED
 
