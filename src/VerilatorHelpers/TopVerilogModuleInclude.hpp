#ifndef TOP_VERILOG_MODULE_INCLUDE 
#define TOP_VERILOG_MODULE_INCLUDE

#define IDENT(x) x
#define XSTR(x) #x
#define STR(x) XSTR(x)
#define PATH(x) STR(IDENT(x))

#include PATH(UT_VERILOG_MODULE_TO_TEST_INCLUDE_PATH)


#endif // TOP_VERILOG_MODULE_INCLUDE
