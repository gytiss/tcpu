
#include <cstdlib>
#include <csignal>
#include <memory>
#include "VerilatorHelpers/TestBench.hpp" 
#include "TopVerilogModuleInclude.hpp"

#ifdef UT_VERILOG_MODULE_TO_TEST_CUSTOM_TEST_BENCH_INCLUDE_PATH
    #include PATH(UT_VERILOG_MODULE_TO_TEST_CUSTOM_TEST_BENCH_INCLUDE_PATH)
#endif

namespace
{
    
#ifdef UT_VERILOG_MODULE_TO_TEST_CUSTOM_TEST_BENCH
    std::unique_ptr<UT_VERILOG_MODULE_TO_TEST_CUSTOM_TEST_BENCH> testBench;
#else
    std::unique_ptr<TestBench<UT_VERILOG_MODULE_TO_TEST>> testBench;
#endif
}

#ifdef UT_VERILATOR_TRACING_ENABLED
// Verilator needs this symbol to be available when linking agains C++ and not SystemC
double sc_time_stamp () 
{       
    uint64_t mainTime = 0; // Current simulation time    
    
    if (testBench.get() != nullptr)
    {
        mainTime = testBench->currentSimulationTimeInTicks();
    }
    
    // Called by $time in Verilog
    return mainTime;
}
#endif

#ifdef UT_VERILOG_TEST_BENCH_IS_CONTINUOUS
namespace
{
    volatile bool terminationRequested = false;
    void signal_handler(int UNUSED_PARAM(signal))
    {
        terminationRequested = true;
    }
}
#endif

int main(int argc, char* argv[])
{
    Verilated::commandArgs(argc, argv);
#ifdef UT_VERILOG_MODULE_TO_TEST_CUSTOM_TEST_BENCH
    testBench.reset(new UT_VERILOG_MODULE_TO_TEST_CUSTOM_TEST_BENCH());
#else
    testBench.reset(new TestBench<UT_VERILOG_MODULE_TO_TEST>());
#endif

#ifdef UT_VERILATOR_TRACING_ENABLED
    testBench->opentrace(PATH(UT_VERILOG_MODULE_TO_TEST_TRACE_FILE_PATH));
#endif

#ifdef UT_VERILOG_TEST_BENCH_IS_CONTINUOUS
    // Install a signal handler
    std::signal(SIGINT, signal_handler);
  
    testBench->continuousTest(terminationRequested);
#else
    testBench->test();
#endif
    
    testBench->final();
    
#ifdef UT_VERILATOR_TRACING_ENABLED
    testBench->closetrace();
#endif

    std::exit(EXIT_SUCCESS);
}
