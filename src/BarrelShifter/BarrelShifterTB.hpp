#ifndef CRC_TB_HPP_INCLUDED 
#define CRC_TB_HPP_INCLUDED

#include "VerilatorHelpers/TestBench.hpp"
#include "VerilatorHelpers/TopVerilogModuleInclude.hpp"

class BarrelShifterTB : public TestBench<UT_VERILOG_MODULE_TO_TEST>
{
public:
    BarrelShifterTB();
    
    virtual ~BarrelShifterTB();

#ifdef UT_VERILOG_TEST_BENCH_IS_CONTINUOUS
    virtual void setupBeforeContinuousTest() override;
#else
    virtual void test() override;
#endif

private:
    bool _leftShiftTest(uint32_t shiftCount);
    bool _rightShiftTest(uint32_t shiftCount);
    
    static void mainClockTick(TestBench<UT_VERILOG_MODULE_TO_TEST>* thisPtr);
};

#endif // CRC_TB_HPP_INCLUDED
 
