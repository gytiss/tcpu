`default_nettype none   // Don't allow undeclared nets

`define ROUND_DOWN_TO_NEAREST_POW_OF_TWO(number) (((((((((((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) | (((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) >> 4))) | (((((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) | (((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) >> 4))) >> 8))) | (((((((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) | (((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) >> 4))) | (((((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) | (((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) >> 4))) >> 8))) >> 16))) + 32'd1)) >> 1)

// Get index of the most significant bit
`define MSB_INDEX(number) ((((((32'd32 - ({31'b0,|(number)} * 32'd1)) - ({31'b0,|((number) & 32'h0000FFFF)} * 32'd16)) - ({31'b0,|((number) & 32'h00FF00FF)} * 32'd8)) - ({31'b0,|((number) & 32'h0F0F0F0F)} * 32'd4)) - ({31'b0,|((number) & 32'h33333333)} * 32'd2)) - ({31'b0,|((number) & 32'h55555555)} * 32'd1))

`define IS_POWER_OF_TWO(number) ( {31'b0, !( !( |( {31'b0, (|(number))} & {31'b0, (!(|((number) & ((number) - 32'd1))))} ) ) )} )
`define MIN_NUM_OF_BITS_FOR_A_NUM(number) (({31'b0, !(|`IS_POWER_OF_TWO((number)))} * `MSB_INDEX(`ROUND_DOWN_TO_NEAREST_POW_OF_TWO((number)))) + (`IS_POWER_OF_TWO((number)) * `MSB_INDEX(number)))


module BarrelShifter
#(parameter OPERATION_WIDTH = 1)
(
    input wire [OPERATION_WIDTH - 1:0] i_operand0,
    inout wire [`MIN_NUM_OF_BITS_FOR_A_NUM(OPERATION_WIDTH):0] i_shiftCount,
    input wire i_shiftRight,
    output reg [OPERATION_WIDTH - 1:0] o_result
);

   // wire [OPERATION_WIDTH - 1:0] w_Zero;

//word[byte_num*8 +: 8]

    //assign o_result = {};

    //assign o_result = {w_Zero ^ i_operand0} | {{(OPERATION_WIDTH - 3){1'b0}}, i_shiftCount};

    //assign w_Zero = {OPERATION_WIDTH{1'b1}};
    


    integer i;

    always_comb
    begin
        o_result = 0;

        for (i = 0; i <= OPERATION_WIDTH; i++) begin
            if (i_shiftCount == i[`MIN_NUM_OF_BITS_FOR_A_NUM(OPERATION_WIDTH):0])
                o_result = (i_shiftRight ? i_operand0 >> i[`MIN_NUM_OF_BITS_FOR_A_NUM(OPERATION_WIDTH):0] : i_operand0 << i[`MIN_NUM_OF_BITS_FOR_A_NUM(OPERATION_WIDTH):0]);
        end

    
//         case (i_shiftCount)
//         generate
//             case (OPERATION_WIDTH)
//             8 : 
//             begin
//                 3'b000 : o_result = i_operand0;
//             end
//         endgenerate
//         endcase
    end

endmodule

// ((((((32'd32 - ({31'b0,|OPERATION_WIDTH} * 32'd1)) - ({31'b0,|(OPERATION_WIDTH & 32'h0000FFFF)} * 32'd16)) - ({31'b0,|(OPERATION_WIDTH & 32'h00FF00FF)} * 32'd8)) - ({31'b0,|(OPERATION_WIDTH & 32'h0F0F0F0F)} * 32'd4)) - ({31'b0,|(OPERATION_WIDTH & 32'h33333333)} * 32'd2)) - ({31'b0,|(OPERATION_WIDTH & 32'h55555555)} * 32'd1))

