cmake_minimum_required (VERSION 3.8)

include(${PROJECT_SOURCE_DIR}/src/VerilatorHelpers/HelperMacros.cmake)

# If verilog modules are using reset signals
set (HAS_RESET_SIGNAL TRUE)

# If it is a one-off unit test or a continuous test
set (CONTINUOUS_TEST FALSE)

# Set tracing file formar. Either "VCD", "FST" or "" if tracing is disabled
set (TRACING_FILE_FORMAT "VCD")
#set (TRACING_FILE_FORMAT "FST")
#set (TRACING_FILE_FORMAT "")

set (CUSTOM_TEST_BENCH_NAME "BarrelShifterTB")

ADD_VERILOG_SOURCE("BarrelShifter_Top.v")
ADD_VERILOG_SOURCE("BarrelShifter.v")
ADD_VERILOG_SOURCE("../CommonVerilog/AsyncResetSynchronizer.v")

ADD_TEST_BENCH_SOURCE("BarrelShifterTB.cpp")

SetupVerilatorGeneration("BarrelShifter_Top" "${VERILOG_SOURCES}" "" "${TRACING_FILE_FORMAT}")
SetupVerilatorUTBuildTarget("BarrelShifter_Top" "${CMAKE_CURRENT_SOURCE_DIR}" "${TEST_BENCH_SOURCES}" ${CUSTOM_TEST_BENCH_NAME} ${HAS_RESET_SIGNAL} ${CONTINUOUS_TEST} "${TRACING_FILE_FORMAT}")
