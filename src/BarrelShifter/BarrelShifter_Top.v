
`define ROUND_DOWN_TO_NEAREST_POW_OF_TWO(number) (((((((((((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) | (((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) >> 4))) | (((((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) | (((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) >> 4))) >> 8))) | (((((((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) | (((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) >> 4))) | (((((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) | (((((((number)) | (((number) - 32'd1) >> 1))) | (((((number)) | (((number) - 32'd1) >> 1))) >> 2))) >> 4))) >> 8))) >> 16))) + 32'd1)) >> 1)

// Get index of the most significant bit
`define MSB_INDEX(number) ((((((32'd32 - ({31'b0,|(number)} * 32'd1)) - ({31'b0,|((number) & 32'h0000FFFF)} * 32'd16)) - ({31'b0,|((number) & 32'h00FF00FF)} * 32'd8)) - ({31'b0,|((number) & 32'h0F0F0F0F)} * 32'd4)) - ({31'b0,|((number) & 32'h33333333)} * 32'd2)) - ({31'b0,|((number) & 32'h55555555)} * 32'd1))

`define IS_POWER_OF_TWO(number) ( {31'b0, !( !( |( {31'b0, (|(number))} & {31'b0, (!(|((number) & ((number) - 32'd1))))} ) ) )} )
`define MIN_NUM_OF_BITS_FOR_A_NUM(number) (({31'b0, !(|`IS_POWER_OF_TWO((number)))} * `MSB_INDEX(`ROUND_DOWN_TO_NEAREST_POW_OF_TWO((number)))) + (`IS_POWER_OF_TWO((number)) * `MSB_INDEX(number)))

module BarrelShifter_Top 
#(parameter WIDTH = 32)
( 
    input wire i_clock, 
    input wire i_asyncRSTn,
    
    input wire [WIDTH - 1:0] i_operand0,
    inout wire [`MIN_NUM_OF_BITS_FOR_A_NUM(WIDTH):0] i_shiftCount,
    input wire i_shiftRight,
    output wire [WIDTH - 1:0] o_result
);
/* verilator lint_off UNUSED */
    wire rst_n;
/* verilator lint_on UNUSED */

    AsyncResetSynchronizer AsyncRstSync
    (
        .i_clock(i_clock),
        .i_asyncRSTn(i_asyncRSTn),
        .o_RSTn(rst_n)
    );

    BarrelShifter #(.OPERATION_WIDTH(WIDTH)) shifter 
    (
        .i_operand0(i_operand0),
        .i_shiftCount(i_shiftCount),
        .i_shiftRight(i_shiftRight),
        .o_result(o_result)
    );

endmodule 
 
