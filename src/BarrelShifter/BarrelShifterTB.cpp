#include "BarrelShifterTB.hpp"
#include <iostream>


enum class ClockID : uint32_t 
{
    Default_25kHz = 0
};

enum class ALUOp : uint8_t 
{
    Add = 0,
    Inc = 1,
    Sub = 2,
    Dec = 3,
    Mul = 4
};


BarrelShifterTB::BarrelShifterTB()
: TestBench<UT_VERILOG_MODULE_TO_TEST>(FreqUnit::kHz, 25, mainClockTick)
{
}

BarrelShifterTB::~BarrelShifterTB() 
{

}

#ifdef UT_VERILOG_TEST_BENCH_IS_CONTINUOUS
void BarrelShifterTB::setupBeforeContinuousTest()
{

}
#else
static uint64_t ShifterOpWidthInBits = 32;

bool BarrelShifterTB::_leftShiftTest(uint32_t shiftCount)
{
    bool retVal = true;

    auto& core = verilogModule();
    
    core.i_shiftRight = 0;
    
    for (size_t i = 0; (i < shiftCount) && retVal; i++)
    {
        core.i_operand0 = 1;
        core.i_shiftCount = i;
        waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));
        retVal = (core.o_result == (((uint64_t)1ull) << i));
    }
    
    return retVal;
}

bool BarrelShifterTB::_rightShiftTest(uint32_t shiftCount)
{
    bool retVal = true;

    auto& core = verilogModule();
    
    core.i_shiftRight = 1;
    
    for (size_t i = 0; (i < shiftCount) && retVal; i++)
    {
        core.i_operand0 = 1;
        core.i_shiftCount = i;
        waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));
        retVal = (core.o_result == (((uint64_t)1ull) >> i));
    }
    
    return retVal;
}



void BarrelShifterTB::test() 
{     
    // Reset verilog logic
    reset(TimeUnit::us, 1);
    
    std::cout << "Left Shift Test  : " << _leftShiftTest(ShifterOpWidthInBits) << std::endl;
    std::cout << "Right Shift Test : " << _rightShiftTest(ShifterOpWidthInBits) << std::endl;

    Verilated::gotFinish(true);  
}
#endif

void BarrelShifterTB::mainClockTick(TestBench<UT_VERILOG_MODULE_TO_TEST>* UNUSED_PARAM(thisPtr))
{
    
}
