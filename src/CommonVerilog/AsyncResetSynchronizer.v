//////////////////////////////////////////////////////
//                                                  //
//   Gate Level Diagram of the Reset Synchronizer   //
//                                                  //
//////////////////////////////////////////////////////
//
//             Pull-up to logic high                  +--------------->>
//             ^                                      |
//             |                                      +--------------->>
//             | +------+    +------+                 |
//             | |      |    |      |      o_RSTn     |
//             --|      |----|      |-----------------+    Reset distribution tree
//               |      |    |      |                 |
//             --|>     |  --|>     |                 |
//             | |      |  | |      |                 +--------------->>
//             | +------+  | +------+                 |
//             |     O     |     O                    +--------------->>
//     i_clock |     |     |     |
//  >>---------+-----|-----+     |
//                   |           |
//     i_asyncRSTn   |           |
//  >>---------------+-----------+
//

module AsyncResetSynchronizer 
( 
    input wire i_clock, 
    input wire i_asyncRSTn,
    output reg o_RSTn 
);
    reg flipFlop1;  
    always @(posedge i_clock or negedge i_asyncRSTn)
    begin
        if (!i_asyncRSTn)
            begin
                {o_RSTn,flipFlop1} <= 2'b0;
            end
        else
            begin
                {o_RSTn,flipFlop1} <= {flipFlop1,1'b1};
            end
    end

endmodule 
