#include "ALUTB.hpp"
#include <iostream>


enum class ClockID : uint32_t 
{
    Default_25kHz = 0
};

enum class ALUOp : uint8_t 
{
    Add = 0,
    Inc = 1,
    Sub = 2,
    Dec = 3,
    Mul = 4,
    XOr = 8,
    Or = 16,
    And = 32,
    Not = 64
};

ALUTB::ALUTB()
: TestBench<UT_VERILOG_MODULE_TO_TEST>(FreqUnit::kHz, 25, mainClockTick)
{
}

ALUTB::~ALUTB() 
{

}

#ifdef UT_VERILOG_TEST_BENCH_IS_CONTINUOUS
void ALUTB::setupBeforeContinuousTest()
{

}
#else
static uint64_t ALUOpWidthInBits = 32;

static uint64_t truncateResult(uint64_t result)
{
    return (result & (0xFFFFFFFFFFFFFFFFull >> (64u - ALUOpWidthInBits)));
}

bool ALUTB::_additionTest(uint64_t a, uint64_t b, bool resultIsNegative, bool carryIsExpected, bool zeroResultIsExpected, bool overflowIsExpected)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = b;
    core.i_operation = static_cast<uint8_t>(ALUOp::Add);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));

    return (((core.o_result == truncateResult(a + b)) && (resultIsNegative ? core.o_resultIsNegative == 1u : core.o_resultIsNegative == 0u) && (carryIsExpected ? core.o_carry == 1u : core.o_carry == 0u)) && (zeroResultIsExpected ? core.o_resultIsZero == 1u : core.o_resultIsZero == 0u) && (overflowIsExpected ? core.o_overflow == 1u : core.o_overflow == 0u));
}

bool ALUTB::_subtractionTest(uint64_t a, uint64_t b, bool resultIsNegative, bool borrowIsExpected, bool overflowIsExpected)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = b;
    core.i_operation = static_cast<uint8_t>(ALUOp::Sub);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));
    
    return ((core.o_result == truncateResult(a - b)) && (resultIsNegative ? core.o_resultIsNegative == 1u : core.o_resultIsNegative == 0u) && (borrowIsExpected ? core.o_borrow == 1u : core.o_borrow == 0u) && (overflowIsExpected ? core.o_overflow == 1u : core.o_overflow == 0u));
}

bool ALUTB::_incrementTest(uint64_t a)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = 0xFF;
    core.i_operation = static_cast<uint8_t>(ALUOp::Inc);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));

    return (core.o_result == truncateResult(a + 1));
}

bool ALUTB::_decrementTest(uint64_t a)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = 0xFF;
    core.i_operation = static_cast<uint8_t>(ALUOp::Dec);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));

    return (core.o_result == truncateResult(a - 1));
}

bool ALUTB::_multiplicationTest(uint64_t a, uint64_t b, bool resultIsNegative)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = b;
    core.i_operation = static_cast<uint8_t>(ALUOp::Mul);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));

    return ((core.o_result == truncateResult(a * b)) && (resultIsNegative ? core.o_resultIsNegative == 1u : core.o_resultIsNegative == 0u));
}

bool ALUTB::_xorTest(uint64_t a, uint64_t b)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = b;
    core.i_operation = static_cast<uint8_t>(ALUOp::XOr);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));

    return (core.o_result == truncateResult(a ^ b));
}

bool ALUTB::_orTest(uint64_t a, uint64_t b)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = b;
    core.i_operation = static_cast<uint8_t>(ALUOp::Or);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));

    return (core.o_result == truncateResult(a | b));
}

bool ALUTB::_andTest(uint64_t a, uint64_t b)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = b;
    core.i_operation = static_cast<uint8_t>(ALUOp::And);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));

    return (core.o_result == truncateResult(a & b));
}

bool ALUTB::_notTest(uint64_t a)
{
    auto& core = verilogModule();
    
    core.i_operand0 = a;
    core.i_operand1 = 0xFFFFFFFF;
    core.i_operation = static_cast<uint8_t>(ALUOp::Not);
    waitForNTicks(1, static_cast<uint32_t>(ClockID::Default_25kHz));

    return (core.o_result == truncateResult(~a));
}

void ALUTB::test() 
{     
    // Reset verilog logic
    reset(TimeUnit::us, 1);

    std::cout << "Addition + Negative + Overflow: " << _additionTest(0x7FFFFFFF, 1, true, false, false, true) << std::endl;
    std::cout << "Addition:                       " << _additionTest(1, 1, false, false, false, false) << std::endl;
    std::cout << "Addition + Carry + Nagative:    " << _additionTest(-1, -2, true, true, false, false) << std::endl;
    std::cout << "Addition + Carry + Nagative:    " << _additionTest(0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, true, true, false, false) << std::endl;
    std::cout << "Addition + Carry + Wraparound:  " << _additionTest(0xFFFFFFFFFFFFFFFF, 1, false, true, true, false) << std::endl;
    std::cout << "Subtraction:                    " << _subtractionTest(2, 1, false, false, false) << std::endl;
    std::cout << "Subtraction + Borrow:           " << _subtractionTest(-2, -1, true, true, false) << std::endl;
    std::cout << "Subtraction + Borrow:           " << _subtractionTest(1, 2, true, true, false) << std::endl;
    std::cout << "Subtraction + Underflow:        " << _subtractionTest(0x80000000, 1, false, false, true) << std::endl;
    std::cout << "Increment:                     " << _incrementTest(1) << std::endl;
    std::cout << "Decrement:                     " << _decrementTest(1) << std::endl;
    std::cout << "Multiplication:                " << _multiplicationTest(0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, false) << std::endl;
    std::cout << "Multiplication:                " << _multiplicationTest(-2, -3, false) << std::endl;
    std::cout << "Multiplication:                " << _multiplicationTest(-2, 3, true) << std::endl;
                                                 
    std::cout << "Multiplication:                " << _multiplicationTest(0x40000000, 2, true) << std::endl;
    std::cout << "Multiplication:                " << _multiplicationTest(0xFFFF, 0x10001, true) << std::endl;
    std::cout << "Multiplication:                " << _multiplicationTest(0xFFFFFFF, 0x8, false) << std::endl;
    

    std::cout << "XOr: " <<  (_xorTest(0xFF00FF0F, 0x00FF00FF) && _xorTest(0xFFFFFFFF, 0xFFFFFFFF) && _xorTest(0x00000000, 0x00000000)) << std::endl;
    std::cout << "Or:  "  <<  (_orTest(0xFF, 0x00) && _orTest(0x00, 0x00) && _orTest(0xFF, 0xFF))  << std::endl;
    std::cout << "And: " <<  (_andTest(0xFF00FF00, 0x00FF00FF) && _andTest(0xFFFFFFFF, 0xFFFFFFFF) && _andTest(0x00000000, 0x00000000)) << std::endl;
    std::cout << "Not: " <<  _notTest(0xAAAAAAAA)             << std::endl;

    Verilated::gotFinish(true);  
}
#endif

void ALUTB::mainClockTick(TestBench<UT_VERILOG_MODULE_TO_TEST>* UNUSED_PARAM(thisPtr))
{
    
}
