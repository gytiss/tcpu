`default_nettype none   // Don't allow undeclared nets

/*
+-------------------+----+-----------+
| F6 F5 F4 F3 F2 F1 | F0 | Operation |
+-------------------+----+-----------+
| 0  0  0  0  0  0  | 0  | A + B     |
| 0  0  0  0  0  0  | 1  | A + 1     |
| 0  0  0  0  0  1  | 0  | A - B     |
| 0  0  0  0  0  1  | 1  | A - 1     |
| 0  0  0  0  1  0  | X  | A * B     |
| 0  0  0  1  0  0  | X  | A xor B   |
| 0  0  1  0  0  0  | X  | A or B    |
| 0  1  0  0  0  0  | X  | A and B   |
| 1  0  0  0  0  0  | X  | not A     |
+-------------------+----+-----------+
*/

module ALU
#(parameter OPERATION_WIDTH = 0)
(
    input wire [OPERATION_WIDTH - 1:0] i_operand0,
    inout wire [OPERATION_WIDTH - 1:0] i_operand1,
    input wire [6:0] i_operation,
    output wire [OPERATION_WIDTH - 1:0] o_result,
    output wire o_carry,
    output wire o_borrow,
    output wire o_overflow,
    output wire o_resultIsZero,
    output wire o_resultIsNegative
);
    wire [OPERATION_WIDTH - 1:0] w_addMuxOut;
    wire [OPERATION_WIDTH - 1:0] w_subMuxOut;
    wire [OPERATION_WIDTH - 1:0] w_addOut;
    wire [OPERATION_WIDTH - 1:0] w_subOut;
    wire [OPERATION_WIDTH - 1:0] w_mulOut;

    wire [OPERATION_WIDTH - 1:0] w_xorOut;
    wire [OPERATION_WIDTH - 1:0] w_orOut;
    wire [OPERATION_WIDTH - 1:0] w_andOut;
    wire [OPERATION_WIDTH - 1:0] w_notOut;
    
    assign w_xorOut = i_operand0 ^ i_operand1;
    assign w_orOut  = i_operand0 | i_operand1;
    assign w_andOut = i_operand0 & i_operand1;
    assign w_notOut = ~i_operand0;
    
    wire w_carry;
    wire w_borrow;
    
    // Carry is only valid for addition
    assign o_carry = (i_operation[6:1] == 6'b000000 ? w_carry : 1'b0);
    
    // Borrow is only valid for subtraction
    assign o_borrow = (i_operation[6:1] == 6'b000001 ? w_borrow : 1'b0);

    assign o_overflow = i_operand0[31] ^ i_operand1[31] ^ o_result[31] ^ (o_carry | o_borrow);
    
    //assign o_overflow = (i_operand0[31] & i_operand1[31] & (~o_result[31])) | ((~i_operand0[31]) & (~i_operand1[31]) & o_result[31]);
    
    assign o_resultIsZero = ~(|o_result);
    assign o_resultIsNegative = o_result[31];

    
    MUX2 #(.WIDTH(OPERATION_WIDTH)) addMux
    (
        .select(i_operation[0]),
        .in0(i_operand1),
        .in1({{(OPERATION_WIDTH - 1){1'b0}}, 1'b1}),
        .out(w_addMuxOut)
    );
    MUX2 #(.WIDTH(OPERATION_WIDTH)) subMux
    (   
        .select(i_operation[0]),
        .in0(i_operand1),
        .in1({{(OPERATION_WIDTH - 1){1'b0}}, 1'b1}),
        .out(w_subMuxOut)    
    );
    
    Adder #(.WIDTH(OPERATION_WIDTH)) adder
    (
        .i_a(i_operand0),
        .i_b(w_addMuxOut),
        .o_sum(w_addOut),
        .o_carry(w_carry)
    );
    Subtracter #(.WIDTH(OPERATION_WIDTH)) sub
    (
        .i_a(i_operand0),
        .i_b(w_subMuxOut),
        .o_diff(w_subOut),
        .o_borrow(w_borrow)
    );
    Multiplier #(.WIDTH(OPERATION_WIDTH)) mul
    (
        .i_a(i_operand0[OPERATION_WIDTH - 1:0]),
        .i_b(i_operand1[OPERATION_WIDTH - 1:0]),
        .o_prod(w_mulOut)
    );
    
    MUX7 #(.WIDTH(OPERATION_WIDTH)) resMux
    (
        .select(i_operation[6:1]),
        .in0(w_addOut),
        .in1(w_subOut),
        .in2(w_mulOut),
        
        .in3(w_xorOut),
        .in4(w_orOut),
        .in5(w_andOut),
        .in6(w_notOut),

        .out(o_result)
    );

endmodule
