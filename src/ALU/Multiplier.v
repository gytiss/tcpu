`default_nettype none   // Don't allow undeclared nets

module Multiplier
#(parameter WIDTH = 1)
(
    input wire [WIDTH - 1:0] i_a,
    input wire [WIDTH - 1:0] i_b,
    output wire [WIDTH - 1:0] o_prod
);
    // Unsigned multiply
    assign o_prod = (i_a * i_b);

endmodule
