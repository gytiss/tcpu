`default_nettype none   // Don't allow undeclared nets

module MUX3
#(parameter WIDTH = 0)
(
    input wire [1:0] select,
    input wire [WIDTH - 1:0] in0,
    input wire [WIDTH - 1:0] in1,
    input wire [WIDTH - 1:0] in2,
    output reg [WIDTH - 1:0] out
);
    always_comb
    begin
        case (select)
            2'b00 : out = in0;
            2'b01 : out = in1;
            2'b10 : out = in2;
            default : out = {WIDTH{1'b0}};
        endcase
    end
endmodule 
