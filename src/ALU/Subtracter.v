`default_nettype none   // Don't allow undeclared nets

module Subtracter
#(parameter WIDTH = 1)
(
    input wire [WIDTH - 1:0] i_a,
    input wire [WIDTH - 1:0] i_b,
    output wire [WIDTH - 1:0] o_diff,
    output wire o_borrow
);

     assign {o_borrow, o_diff} = (i_a - i_b);

endmodule
