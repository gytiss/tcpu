`default_nettype none   // Don't allow undeclared nets

module Adder
# (parameter WIDTH = 1)
(
    input wire [WIDTH - 1:0] i_a,
    input wire [WIDTH - 1:0] i_b,
    output wire [WIDTH - 1:0] o_sum,
    output wire o_carry
);

    assign {o_carry, o_sum} = (i_a + i_b);

endmodule
