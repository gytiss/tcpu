`default_nettype none   // Don't allow undeclared nets

module MUX7
#(parameter WIDTH = 0)
(
    input wire [5:0] select,
    input wire [WIDTH - 1:0] in0,
    input wire [WIDTH - 1:0] in1,
    input wire [WIDTH - 1:0] in2,
    input wire [WIDTH - 1:0] in3,
    input wire [WIDTH - 1:0] in4,
    input wire [WIDTH - 1:0] in5,
    input wire [WIDTH - 1:0] in6,

    output reg [WIDTH - 1:0] out
);
    always_comb
    begin
        case (select)
            6'b000000 : out = in0;
            6'b000001 : out = in1;
            6'b000010 : out = in2;
            6'b000100 : out = in3;
            6'b001000 : out = in4;
            6'b010000 : out = in5;
            6'b100000 : out = in6;
            default : out = {WIDTH{1'b0}};
        endcase
    end
endmodule 
