`default_nettype none   // Don't allow undeclared nets

module MUX2
#(parameter WIDTH = 1)
(
    input wire select,
    input wire [WIDTH - 1:0] in0,
    input wire [WIDTH - 1:0] in1,
    output wire [WIDTH - 1:0] out
);

    assign out = (select == 1'b0 ? in0 : in1);

endmodule
