module ALU_Top 
#(parameter WIDTH = 32)
( 
    input wire i_clock, 
    input wire i_asyncRSTn,
    
    input wire [WIDTH - 1:0] i_operand0,
    inout wire [WIDTH - 1:0] i_operand1,
    input wire [6:0] i_operation,
    output wire [WIDTH - 1:0] o_result,
    output wire o_carry,
    output wire o_borrow,
    output wire o_overflow,
    output wire o_resultIsZero,
    output wire o_resultIsNegative
);
/* verilator lint_off UNUSED */
    wire rst_n;
/* verilator lint_on UNUSED */

    AsyncResetSynchronizer AsyncRstSync
    (
        .i_clock(i_clock),
        .i_asyncRSTn(i_asyncRSTn),
        .o_RSTn(rst_n)
    );

    ALU #(.OPERATION_WIDTH(WIDTH)) alu
    (
        .i_operand0(i_operand0),
        .i_operand1(i_operand1),
        .i_operation(i_operation),
        .o_result(o_result),
        .o_carry(o_carry),
        .o_borrow(o_borrow),
        .o_overflow(o_overflow),
        .o_resultIsZero(o_resultIsZero),
        .o_resultIsNegative(o_resultIsNegative)
    );

endmodule 
 
